//
//  ApiHandler.swift
//  AIPL ABRO
//
//  Created by CST on 21/01/20.
//  Copyright © 2020 promatics. All rights reserved.
//

import Foundation
import SwiftyJSON

class  ApiHandler:Requestable{
    
    func fetchApiService(method:Method,url:String,passDict:[String:Any]? = nil,header:[String:String]? = nil, callback: @escaping Handler){
        
        request(method: method, url: url, params: passDict, headers: header) {(result) in
            
            callback(result)
            print(result.self)
            Indicator.shared.hideProgressView()
        }
        
    }
    
    
    func fetchMultipartedApiService(imageData : [NSData]? = nil ,fileName:[String]? = nil , imageparams:[String]? = nil,url:String,params:[String:Any]? = nil , headers:[String:String]? = nil , callback:@escaping Handler){
        
        multipartingRequest(imageData: imageData, fileName: fileName, imageParam: imageparams, url: url, params: params, headers: headers) { (result) in
            
            callback(result)
            Indicator.shared.hideProgressView()
        }
       
    }
    
    
}
