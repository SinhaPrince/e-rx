//
//  MapInfoWindow.swift
//  GoogleMapsCustomInfoWindow
//
//  Created by Sofía Swidarowicz Andrade on 11/5/17.
//  Copyright © 2017 Sofía Swidarowicz Andrade. All rights reserved.
//

import UIKit

class MapInfoWindow: UIView {

    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var titleInfo: UILabel!
    @IBOutlet weak var addressInfo: UILabel!
    @IBOutlet weak var mobileInfo: UILabel!
    @IBOutlet weak var MainView: UIView!
    
    var item: NearByProviderData? {
        didSet {
            guard let data = item else {
                return
            }
            comClass.LblFontSizeName(name: data.fullName ?? "", lbl: titleInfo, textColor: AppColor.whiteColor, fontName: comClass.BoldFont, fontSize: 16)
            comClass.LblFontSizeName(name: data.address ?? "", lbl: addressInfo, textColor: AppColor.whiteColor, fontName: comClass.BoldFont, fontSize: 16)
            comClass.LblFontSizeName(name: data.mobileNumber ?? "", lbl: mobileInfo, textColor: AppColor.whiteColor, fontName: comClass.BoldFont, fontSize: 16)
            imgInfo.setImage(withImageId: data.profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        }
    }
    
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapInfoWindowView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
   
    
}
