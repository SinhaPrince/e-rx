//
//  WarningMessageView.swift
//  E-RX
//
//  Created by SinhaAirBook on 24/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class WarningMessageView: UIView {

    //MARK:- Outlet
    @IBOutlet weak var lbl_Message: UILabel!
    @IBOutlet weak var tranView: UIView!
    //MARK:- Variable
    var delegate : TryAgainDeleagte?

    var message : String?{
        didSet{
            guard let message = message else{
                return
            }
            comClass.LblFontSizeName(name: message, lbl: lbl_Message, textColor: AppColor.whiteColor, fontName: comClass.BoldFont, fontSize: 17)
           
        }
    }
    
    @IBAction func TryAgain(_ sender: UIButton) {
       
        self.delegate?.tryAgain()
        
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "WarningMessageView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}

protocol TryAgainDeleagte {
    
    func tryAgain()
    
}

protocol LoadWarningMessageView {
    
    func loadWarinigView()
    
}
