//
//  AppUrl.swift
//  E-RX
//
//  Created by SinhaAirBook on 29/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

struct Domain {
    
    static let development = "http://3.22.227.41:2020/api/v1/user/"
    static let socketUrl = "http://3.22.227.41:2020"
    static let chatHistory = "http://3.22.227.41:2020/api/v1/user/"

}
extension Domain {
    static func baseUrl() -> String {
        return Domain.development
    }
    static func socketURL() -> String{
        return Domain.socketUrl
    }
    static func chatHistoryUrl() ->String{
        return Domain.chatHistory
    }
}

struct APIEndpoint {
    static let signInApi         = "userLogin"
    static let prescriptionRequest = "prescriptionRequest"
    static let getNotificationList  = "getNotificationList"
    static let clearNotification = "clearNotification"
    static let userSignup        = "userSignup"
    static let userUpdateDetails = "userUpdateDetails"
    static let checkEmailAndMobileAvailability   = "checkEmailAndMobileAvailability"
    static let checkMobileForForgotPassword = "checkMobileForForgotPassword"
    static let forgotPassword = "forgotPassword"
    static let userLogout      = "userLogout"
    static let changeMobileNumber = "changeMobileNumber"
    static let checkMobileAvailability = "checkMobileAvailability"
    static let prescriptionOfferList = "prescriptionOfferList"
    static let getChatHistory = "getChatHistory"
    static let acceptPrescriptionRequest = "acceptPrescriptionRequest"
    static let prescriptionRequestReject = "prescriptionRequestReject"
    static let changeNotificationStatus = "changeNotificationStatus"
    static let nearByProviderList = "nearByProviderList"
    static let chargeApi = "chargeApi"
    static let paymentApi = "paymentApi"
    static let checkEmailForForgotPassword = "checkEmailForForgotPassword"
}


enum HTTPHeaderField: String {
    
    case authentication  = "Authorization"
    case contentType     = "Content-Type"
    case acceptType      = "Accept"
    case acceptEncoding  = "Accept-Encoding"
    case acceptLangauge  = "Accept-Language"
    
    var header:String{
        
        switch self {
        case .authentication:
            return "Authorization"
        case .contentType:
            return "Content-Type"
        case .acceptType:
            return "Accept"
        case .acceptEncoding:
            return "Accept-Encoding"
        case .acceptLangauge:
            return "Accept-Language"
        
        }
        
    }
    
}

enum ContentType: String {
    case json            = "application/json"
    case multipart       = "multipart/form-data"
    case ENUS            = "en-us"
}

enum MultipartType: String {
    case image = "Image"
    case csv = "CSV"
}

enum MimeType: String {
    case image = "image/png"
    case csvText = "text/csv"
}

enum UploadType: String {
    case avatar
    case file
}



