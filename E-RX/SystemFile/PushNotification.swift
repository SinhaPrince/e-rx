//
//  PushNotification.swift
//  E-RX
//
//  Created by SinhaAirBook on 11/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//
import UIKit
import UserNotifications
import Firebase
import FirebaseAuth
import FirebaseMessaging

class PushNotification:NSObject, UNUserNotificationCenterDelegate,MessagingDelegate {
    
    
    public init(application:UIApplication) {
        
        super.init()
 
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(
        options: [.alert, .sound, .badge]) {
            [weak self] granted, error in
            print("Notification granted: \(granted)")
            guard granted else { return }
            self?.getNotificationSettings()
        }
        
        FirebaseApp.configure()
        
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
    }
    
    required init(coder aDecoder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("User Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func userNotification(message: String, title: String, count: Int){
        //creating the notification content
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = ""
        content.body = message
        content.badge = count as NSNumber
        //getting the notification trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func clearNotification(){
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let notifications =  UNUserNotificationCenter.current()
        notifications.removeAllPendingNotificationRequests()
        notifications.removeAllDeliveredNotifications()
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let userInfo = notification.request.content.userInfo as? [String:Any] {
            if let apsDict = userInfo["aps"] as?NSDictionary {
                let badge = apsDict.value(forKey: "badge") as? Int ?? 0
                UIApplication.shared.applicationIconBadgeNumber = badge
                completionHandler([.alert,.badge,.sound])
            }
            
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInfo = response.notification.request.content.userInfo as? [String:Any] {
            if let apsDict = userInfo["aps"] as?NSDictionary {
                let badge = apsDict.value(forKey: "badge") as? Int ?? 0
                UIApplication.shared.applicationIconBadgeNumber = badge
                completionHandler()
            }
            
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    }
    
}

