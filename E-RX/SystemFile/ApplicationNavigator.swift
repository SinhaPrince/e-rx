//
//  ApplicationNavigator.swift
//  E-RX
//
//  Created by SinhaAirBook on 10/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import KYDrawerController
import RealmSwift

class ApplicationNavigator {
    
    //MARK:- Variables
    var drawerController = KYDrawerController.init(drawerDirection: Localize.currentLanguage() == "en" ? .left : .right, drawerWidth: UIScreen.main.bounds.width - 75)
    //MARK:- initializer
    init(window: UIWindow? = nil,sendViewController:String? = nil) {
        
        let realm = try! Realm()
        let scope = realm.objects(AppStart.self)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if scope.first?.isLanguageScreenShow == "Yes"{
            
            let userData = realm.objects(UserDataModel.self)
            
            self.screenOrientation(currentLanguage: Localize.currentLanguage())
            
            if (userData.count != 0){
                
                let mainVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(sendViewController ?? "HomeVC")")
                let drawerVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
                let navigationController = UINavigationController(rootViewController: drawerController)
                self.drawerController.mainViewController = mainVC
                self.drawerController.drawerViewController = drawerVC
                drawerController.setDrawerState(.closed, animated: true)
                navigationController.isNavigationBarHidden = true
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()
                
            }else{
                
                let vc = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                let nav: UINavigationController = UINavigationController(rootViewController: vc)
                nav.isNavigationBarHidden = true
                kAppDelegate.push?.clearNotification()
                window?.rootViewController = nav
                
            }
            
            
        }else{
            
            let vc = storyboard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            nav.isNavigationBarHidden = true
            kAppDelegate.push?.clearNotification()
            window?.rootViewController = nav
            
        }
    }
    
    func screenOrientation(currentLanguage: String){
        if currentLanguage == "en"{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
    }
    
}
