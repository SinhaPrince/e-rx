//
//  AppDelegate.extension.swift
//  ios-messaging-app
//
//  Created by Francisco Igor on 2018-12-07.
//  Copyright © 2018 User. All rights reserved.
//
import UIKit
import Firebase

extension AppDelegate{
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
        ) {
                
        InstanceID.instanceID().instanceID { (result, error) in
            guard error == nil else{
                let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
                let token = tokenParts.joined()
                print("My Device Token: \(token)")
                return
            }
            print("FCM Token : \(result?.token ?? "")")
            UserDefaults.standard.set(result?.token ?? "", forKey: "DeviceToken")
        }
        
    }

    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        let simulaterToken = "Simulaterwalatokenbb55d44bfc4492bd33aac79afeaee474e92c12138e18b021e2326"
               
        UserDefaults.standard.set(simulaterToken, forKey: "DeviceToken")
        print("Error registering notifications: \(error)")
    }
    
  
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    
}

