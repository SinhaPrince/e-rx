//
//  AppDelegate.swift
//  E-RX
//
//  Created by macbook on 28/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigator: ApplicationNavigator?
    var push : PushNotification?
    let gcmMessageIDKey = "erx.message"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        UINavigationBar().appearen()
        //IQKeyboard Manager setup
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.black
        
        // Google Key Registration
        GMSServices.provideAPIKey("AIzaSyBLFp6z5ultFP4vY7piiD9bQQmVJ7HyOEM")
        GMSPlacesClient.provideAPIKey("AIzaSyBLFp6z5ultFP4vY7piiD9bQQmVJ7HyOEM")
    
        // Register Notification
        push = PushNotification(application: application)
        
        // For Auto Login
        window = UIWindow(frame: UIScreen.main.bounds)
        navigator = ApplicationNavigator(window: window, sendViewController: "HomeVC")
        
        return true
    }
    
    
}


