

import Foundation
import SwiftyJSON

struct NotificationData :Equatable {
    
    let isSeen : Bool?
	let _id : String?
	let notiToUser : String?
	let notiTitle : String?
	let notiMessage : String?
	let createdAt : String?
	let updatedAt : String?
	let __v : Int?

    
//    init(jsonData:[String:Any]) {
//        
//        isSeen = jsonData["isSeen"] as? Bool ?? false
//        _id = jsonData["_id"] as? String ?? ""
//        notiTitle = jsonData["notiTitle"] as? String ?? ""
//        notiToUser = jsonData["notiToUser"] as? String ?? ""
//        notiMessage = jsonData["notiMessage"] as? String ?? ""
//        createdAt = jsonData["createdAt"] as? String ?? ""
//        updatedAt = jsonData["updatedAt"] as? String ?? ""
//        __v = jsonData["__v"] as? Int ?? 0
//    }
   
    init(_ json: JSON) {
        isSeen = json["isSeen"].boolValue
        _id = json["_id"].stringValue
        notiTitle = json["notiTitle"].stringValue
        notiToUser = json["notiToUser"].stringValue
        notiMessage = json["notiMessage"].stringValue
        createdAt = json["createdAt"].stringValue
        updatedAt = json["updatedAt"].stringValue
        __v = json["__v"].intValue
    }
    
}
