

import Foundation
import SwiftyJSON

struct  NotificationBaseModel:Equatable  {
    
	var status : Int?
	var message : String?
    var data : [NotificationData]?

    init(json:JSON) {
        
        status = json["status"].int
        message = json["message"].string
        //data = [NotificationData]()
        data = json["data"].arrayValue.map { NotificationData($0) }
        
//        if let dataArray = json["data"].arrayObject as? [[String:Any]]{
//            for dic in dataArray{
//                let value = NotificationData(jsonData: dic)
//                data.append(value)
//            }
//        }
    }

}
