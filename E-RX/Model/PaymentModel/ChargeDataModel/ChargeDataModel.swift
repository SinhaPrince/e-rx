//
//  ChargeDataModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ChargeDataModel {

	let code: Int?
	let message: String?
	let data: ChargePayData?

	init(_ json: JSON) {
		code = json["code"].intValue
		message = json["message"].stringValue
		data = ChargePayData(json["data"])
	}

}
