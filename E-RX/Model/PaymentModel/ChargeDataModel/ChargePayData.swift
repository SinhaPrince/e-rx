//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ChargePayData {

	let providerAmount: Int?
	let adminAmount: Double?
	let finalAmount: Double?
    let deliveryToAdminAmount: Double?
    let companyAmount: Double?
	let chargeData: ChargeData?

	init(_ json: JSON) {
		providerAmount = json["providerAmount"].intValue
		adminAmount = json["adminAmount"].doubleValue
		finalAmount = json["finalAmount"].doubleValue
		chargeData = ChargeData(json["chargeData"])
        companyAmount = json["companyAmount"].doubleValue
        deliveryToAdminAmount = json["deliveryToAdminAmount"].doubleValue
	}

}
