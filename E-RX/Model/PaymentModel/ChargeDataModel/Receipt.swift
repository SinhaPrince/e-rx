//
//  Receipt.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ReceiptData {

	let email: Bool?
	let sms: Bool?

	init(_ json: JSON) {
		email = json["email"].boolValue
		sms = json["sms"].boolValue
	}

}
