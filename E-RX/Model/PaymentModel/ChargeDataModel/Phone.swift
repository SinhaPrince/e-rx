//
//  Phone.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct Phone {

	let countryCode: String?
	let number: String?

	init(_ json: JSON) {
		countryCode = json["country_code"].stringValue
		number = json["number"].stringValue
	}

}