//
//  Transaction.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct TransactionData {

	let timezone: String?
	let created: String?
	let url: String?
	let expiry: ExpiryData?
	let asynchronous: Bool?
	let amount: Double?
	let currency: String?

	init(_ json: JSON) {
		timezone = json["timezone"].stringValue
		created = json["created"].stringValue
		url = json["url"].stringValue
		expiry = ExpiryData(json["expiry"])
		asynchronous = json["asynchronous"].boolValue
		amount = json["amount"].doubleValue
		currency = json["currency"].stringValue
	}

}
