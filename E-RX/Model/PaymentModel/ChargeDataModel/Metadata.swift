//
//  Metadata.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct MetadataData {

	let udf1: String?
	let udf2: String?

	init(_ json: JSON) {
		udf1 = json["udf1"].stringValue
		udf2 = json["udf2"].stringValue
	}

}
