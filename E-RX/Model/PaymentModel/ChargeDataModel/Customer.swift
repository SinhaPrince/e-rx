//
//  Customer.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct CustomerData {

	let firstName: String?
	let email: String?
	let phone: Phone?

	init(_ json: JSON) {
		firstName = json["first_name"].stringValue
		email = json["email"].stringValue
		phone = Phone(json["phone"])
	}

}
