//
//  Expiry.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ExpiryData {

	let period: Int?
	let type: String?

	init(_ json: JSON) {
		period = json["period"].intValue
		type = json["type"].stringValue
	}

}
