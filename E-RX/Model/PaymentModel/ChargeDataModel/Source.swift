//
//  Source.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct SourceData {

	let object: String?
	let id: String?

	init(_ json: JSON) {
		object = json["object"].stringValue
		id = json["id"].stringValue
	}

}
