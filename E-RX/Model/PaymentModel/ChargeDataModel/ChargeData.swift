//
//  ChargeData.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ChargeData {

	let id: String?
	let object: String?
	let liveMode: Bool?
	let apiVersion: String?
	let method: String?
	let status: String?
	let amount: Double?
	let currency: String?
	let threeDSecure: Bool?
	let cardThreeDSecure: Bool?
	let saveCard: Bool?
	let statementDescriptor: String?
	let description: String?
	let metadata: MetadataData?
	let transaction: TransactionData?
	let reference: ReferenceData?
	let response: ResponseData?
	let receipt: ReceiptData?
	let customer: CustomerData?
	let source: SourceData?
	let redirect: RedirectData?
	let post: PostData?

	init(_ json: JSON) {
		id = json["id"].stringValue
		object = json["object"].stringValue
		liveMode = json["live_mode"].boolValue
		apiVersion = json["api_version"].stringValue
		method = json["method"].stringValue
		status = json["status"].stringValue
		amount = json["amount"].doubleValue
		currency = json["currency"].stringValue
		threeDSecure = json["threeDSecure"].boolValue
		cardThreeDSecure = json["card_threeDSecure"].boolValue
		saveCard = json["save_card"].boolValue
		statementDescriptor = json["statement_descriptor"].stringValue
		description = json["description"].stringValue
		metadata = MetadataData(json["metadata"])
		transaction = TransactionData(json["transaction"])
		reference = ReferenceData(json["reference"])
		response = ResponseData(json["response"])
		receipt = ReceiptData(json["receipt"])
		customer = CustomerData(json["customer"])
		source = SourceData(json["source"])
		redirect = RedirectData(json["redirect"])
		post = PostData(json["post"])
	}

}
