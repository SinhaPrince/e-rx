//
//  Response.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ResponseData {

	let code: String?
	let message: String?

	init(_ json: JSON) {
		code = json["code"].stringValue
		message = json["message"].stringValue
	}

}
