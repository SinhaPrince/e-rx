//
//  Redirect.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct RedirectData {

	let status: String?
	let url: String?

	init(_ json: JSON) {
		status = json["status"].stringValue
		url = json["url"].stringValue
	}

}
