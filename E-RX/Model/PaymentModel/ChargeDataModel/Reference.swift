//
//  Reference.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on August 10, 2020
//
import Foundation
import SwiftyJSON

struct ReferenceData {

	let transaction: String?
	let order: String?

	init(_ json: JSON) {
		transaction = json["transaction"].stringValue
		order = json["order"].stringValue
	}

}
