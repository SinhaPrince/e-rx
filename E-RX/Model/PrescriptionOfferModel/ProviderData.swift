//
//  ProviderData.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 10, 2020
//
import Foundation
import SwiftyJSON

struct ProviderData:Equatable {

	let profilePic: String?
	let avgRating: Int?
	let username: String?
	let mobileNumber: String?
	let countryCode: String?
	let fullName: String?

	init(_ json: JSON) {
		profilePic = json["profilePic"].stringValue
		avgRating = json["avgRating"].intValue
		username = json["username"].stringValue
		mobileNumber = json["mobileNumber"].stringValue
		countryCode = json["countryCode"].stringValue
		fullName = json["fullName"].stringValue
	}

}
