//
//  PrescriptionOfferList.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 10, 2020
//
import Foundation
import SwiftyJSON

struct PrescriptionOfferList:Equatable {

	let status: Int?
	let message: String?
	var data: [DataModel]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { DataModel($0) }
	}

}
