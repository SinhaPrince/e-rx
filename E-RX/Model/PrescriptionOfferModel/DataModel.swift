//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 10, 2020
//
import Foundation
import SwiftyJSON

struct DataModel:Equatable {

	let Id: String?
	let userId: String?
	let providerId: String?
	let prescriptionNumber: String?
	let amount: Double?
	let currency: String?
	let createdAt: String?
	let providerData: ProviderData?

	init(_ json: JSON) {
		Id = json["_id"].stringValue
		userId = json["userId"].stringValue
		providerId = json["providerId"].stringValue
		prescriptionNumber = json["prescriptionNumber"].stringValue
        amount = Double(json["amount"].intValue) + 26.25
		currency = json["currency"].stringValue
		createdAt = json["createdAt"].stringValue
		providerData = ProviderData(json["providerData"])
	}

}

