//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2020
//
import Foundation
import SwiftyJSON

struct NearByProviderData:Equatable {

	let Id: String?
	let profilePic: String?
	let username: String?
	let email: String?
	let mobileNumber: String?
	let countryCode: String?
	let address: String?
	let latitude: Double?
	let longitude: Double?
	let tradeLicence: String?
	let fullName: String?
	let tradeExpiryDate: String?
	let dist: Dist?

	init(_ json: JSON) {
		Id = json["_id"].stringValue
		profilePic = json["profilePic"].stringValue
		username = json["username"].stringValue
		email = json["email"].stringValue
		mobileNumber = json["mobileNumber"].stringValue
		countryCode = json["countryCode"].stringValue
		address = json["address"].stringValue
		latitude = json["latitude"].doubleValue
		longitude = json["longitude"].doubleValue
		tradeLicence = json["tradeLicence"].stringValue
		fullName = json["fullName"].stringValue
		tradeExpiryDate = json["tradeExpiryDate"].stringValue
		dist = Dist(json["dist"])
	}

}
