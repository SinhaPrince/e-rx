//
//  Dist.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2020
//
import Foundation
import SwiftyJSON

struct Dist:Equatable {

	let calculated: Int?

	init(_ json: JSON) {
		calculated = json["calculated"].intValue
	}

}
