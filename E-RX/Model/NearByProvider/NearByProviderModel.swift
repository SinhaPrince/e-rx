//
//  NearByProviderModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 21, 2020
//
import Foundation
import SwiftyJSON

struct NearByProviderModel:Equatable {

	let status: Int?
	let message: String?
	let data: [NearByProviderData]?

	init(_ json: JSON) {
		status = json["status"].intValue
		message = json["message"].stringValue
		data = json["data"].arrayValue.map { NearByProviderData($0) }
	}

}
