

import Foundation

struct PriscriptionData : Equatable {
    
	
	let status : String?
	let prescriptionStatus : String?
	let cancelStatus : Bool?
	let deleteStatus : Bool?
	let paymentType : String?
	let assignDriverStatus : Bool?
	let driverAssignRequest : Bool?
	let _id : String?
	let userId : String?
	let prescriptionNumber : String?
	let prescriptionImage : String?
	let latitude : Double?
	let longitude : Double?
	let address : String?
	let year : Int?
	let month : Int?
	let createdAt : String?
	let updatedAt : String?
	let deliveryRoomId : String?
	let deliveryType : String?
	let invoice : String?
	let offerId : String?
	let providerId : ProviderId?
	let providerRoomId : String?
	let companyId : String?
	let deliveryId : DeliveryId?

   
    init(DictData:[String:Any]) {
        
        let providerIdDict = DictData["providerId"] as? [String:Any] ?? [:]
            
        providerId = ProviderId(data:providerIdDict)
        
        let deliveryIdDict = DictData["deliveryId"] as? [String:Any] ?? [:]
            
        deliveryId = DeliveryId(data:deliveryIdDict)
        
        paymentType = DictData["paymentType"] as? String ?? ""
        invoice = DictData["invoice"] as? String ?? ""
        offerId = DictData["offerId"] as? String ?? ""
        providerRoomId = DictData["providerRoomId"] as? String ?? ""
        companyId = DictData["companyId"] as? String ?? ""
        
        deliveryRoomId = DictData["deliveryRoomId"] as? String ?? ""
        deliveryType = DictData["deliveryType"] as? String ?? ""
        updatedAt = DictData["updatedAt"] as? String ?? ""
        createdAt = DictData["createdAt"] as? String ?? ""
        month = DictData["month"] as? Int ?? 0
        year = DictData["year"] as? Int ?? 0
        address = DictData["address"] as? String ?? ""
        longitude = DictData["longitude"] as? Double ?? 0.0
        latitude = DictData["latitude"] as? Double ?? 0.0
        prescriptionImage = DictData["prescriptionImage"] as? String ?? ""
        prescriptionNumber = DictData["prescriptionNumber"] as? String ?? ""
        userId = DictData["userId"] as? String ?? ""
        driverAssignRequest = DictData["driverAssignRequest"] as? Bool ?? false
        _id = DictData["_id"] as? String ?? ""
        assignDriverStatus = DictData["assignDriverStatus"] as? Bool ?? false
        deleteStatus = DictData["deleteStatus"] as? Bool ?? false
        status = DictData["status"] as? String ?? ""
        prescriptionStatus = DictData["prescriptionStatus"] as? String ?? ""
        cancelStatus = DictData["cancelStatus"] as? Bool ?? false
        
    }

    

}
