

import Foundation
import SwiftyJSON

struct PrescriptionBaseModel : Equatable {
    
	let status : Int?
	let message : String?
	var data : [PriscriptionData]!

	

    init(json:JSON) {
        
        status = json["status"].int
        message = json["message"].string
        data = [PriscriptionData]()
        
        if let dataArray = json["data"].arrayObject as? [[String:Any]]{
            for dic in dataArray{
                let value = PriscriptionData(DictData: dic)
                data.append(value)
            }
        }
    }
    

}
