

import Foundation
struct DeliveryId : Equatable {
    
	let _id : String?

    init(data:[String:Any]) {
        
        _id = data["_id"] as? String ?? ""
    }


}
