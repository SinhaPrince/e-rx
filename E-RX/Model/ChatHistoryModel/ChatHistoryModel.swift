//
//  ChatHistoryModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020
//
import Foundation
import SwiftyJSON

struct ChatHistoryModel:Equatable {

	let data: [ChatData]?
	let message: String?
	let status: Int?

	init(_ json: JSON) {
		data = json["data"].arrayValue.map { ChatData($0) }
		message = json["message"].stringValue
		status = json["status"].intValue
	}

}
