//  Data.swift
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020

import Foundation
import SwiftyJSON

struct ChatData:Equatable {

	let updatedAt: String?
	let roomId: String?
	let _v: Int?
	let message: String?
	let Id: String?
	let createdAt: String?
	let senderId: String?
	let receiverId: String?

	init(_ json: JSON) {
		updatedAt = json["updatedAt"].stringValue
		roomId = json["roomId"].stringValue
		_v = json["__v"].intValue
		message = json["message"].stringValue
		Id = json["_id"].stringValue
		createdAt = json["createdAt"].stringValue
		senderId = json["senderId"].stringValue
		receiverId = json["receiverId"].stringValue
	}

}
