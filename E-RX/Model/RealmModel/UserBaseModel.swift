//
//  UserBaseModel.swift
//  E-RX
//
//  Created by SinhaAirBook on 14/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

struct  UserBaseModel  {
    
    var status : Int?
    var message : String?
    var data : [UserDataModel]!
    
    init(json:JSON) {
        
        status = json["status"].int
        message = json["message"].string
        data = [UserDataModel]()
        
        if let dict = json["data"].dictionaryObject{
            
            let value = UserDataModel(data: dict)
                data.append(value)
            let realm = try! Realm()
            
            try! realm.write({
                let deletedNotifications = realm.objects(UserDataModel.self)
                realm.delete(deletedNotifications)
                realm.add(data)
                
            })
        }
    }
    
}

class ATCWalkthroughModel {
    var title: String
    var subtitle: String
    var icon: String
    var btnTitle: String

    init(title: String, subtitle: String, icon: String, btnTitle: String) {
        self.title = title
        self.subtitle = subtitle
        self.icon = icon
        self.btnTitle = btnTitle
    }
}
