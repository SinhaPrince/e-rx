//
//  UserDataModel.swift
//  E-RX
//
//  Created by SinhaAirBook on 12/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import RealmSwift

class UserDataModel:Object{
    
    @objc dynamic var id :String?
    @objc dynamic var userType :String?
    @objc dynamic var profilePic :String?
    @objc dynamic var notificationStatus :String?
    @objc dynamic var totalPrescription :String?
    @objc dynamic var totalCancelPrescription :String?
    @objc dynamic var fullName :String?
    
    @objc dynamic var email :String?
    @objc dynamic var countryCode :String?
    @objc dynamic var mobileNumber :String?
    @objc dynamic var address :String?
    @objc dynamic var deviceType :String?
    @objc dynamic var deviceToken :String?

    @objc dynamic var latitude :String?
    @objc dynamic var longitude :String?
    @objc dynamic var emiratesId :String?
    @objc dynamic var gender :String?
    @objc dynamic var dob :String?
    @objc dynamic var insuranceCardPicture :String?
    
    @objc dynamic var emiratesIdPicture :String?
    @objc dynamic var createdAt :String?
    @objc dynamic var updatedAt :String?
    @objc dynamic var Token :String?

    
 
     convenience init(data:[String:Any]) {
        
        self.init()
        
        totalPrescription = String(data["totalPrescription"] as? Int ?? 0)
        totalCancelPrescription = String(data["totalCancelPrescription"] as? Int ?? 0)
        notificationStatus = String(data["notificationStatus"] as? Bool ?? false)
        
        latitude = String(data["latitude"] as? Double ?? 0.0)
        longitude = String(data["longitude"] as? Double ?? 0.0)
        mobileNumber = data["mobileNumber"] as? String ?? ""
        id = data["_id"] as? String ?? ""
        updatedAt = data["updatedAt"] as? String ?? ""
        
        fullName = data["fullName"] as? String ?? ""
        Token = data["jwtToken"] as? String ?? ""
        gender = data["gender"] as? String ?? ""
        id = data["_id"] as? String ?? ""
        insuranceCardPicture = data["insuranceCardPicture"] as? String ?? ""
        
        dob = data["dob"] as? String ?? ""
        email = data["email"] as? String ?? ""
        emiratesId = data["emiratesId"] as? String ?? ""
        emiratesIdPicture = data["emiratesIdPicture"] as? String ?? ""
        profilePic = data["profilePic"] as? String ?? ""
        
        address = data["address"] as? String ?? ""
        countryCode = data["countryCode"] as? String ?? ""
        createdAt = data["createdAt"] as? String ?? ""
        deviceToken = data["deviceToken"] as? String ?? ""
        deviceType = data["deviceType"] as? String ?? ""
        
        
    }
    
    
}

class AppStart:Object{
    
    @objc dynamic var isLanguageScreenShow : String?

}
class WalkThorughShow: Object {
    
    @objc dynamic var isAppStarted :String?

}

class RememberMeModel:Object{
    @objc dynamic var userName: String?
    @objc dynamic var password: String?
    
    convenience init(userName: String, password: String) {
        self.init()
        self.userName = userName
        self.password = password
    }
    
}
