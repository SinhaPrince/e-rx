//
//  PrescriptionDetailBaseModel.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020
//
import Foundation
import SwiftyJSON

struct PrescriptionDetailBaseModel:Equatable {

	let status: Int?
	let data: PrescriptionDetailData?
	let message: String?

	init(_ json: JSON) {
		status = json["status"].intValue
		data = PrescriptionDetailData(json["data"])
		message = json["message"].stringValue
	}

}
