//
//  Data.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020
//
import Foundation
import SwiftyJSON

struct PrescriptionDetailData:Equatable {

	let prescriptionNumber: String?
	let cancelStatus: Bool?
	let createdAt: String?
	let deliveryType: String?
	let driverAssignRequest: Bool?
	let updatedAt: String?
	let userId: String?
	let providerRoomId: String?
	let prescriptionImage: String?
	let longitude: Double?
	let providerId: ProviderDetailId?
	let prescriptionStatus: String?
	let year: Int?
	let _v: Int?
	let invoice: String?
	let deleteStatus: Bool?
	let offerId: String?
	let companyId: String?
	let assignDriverStatus: Bool?
	let latitude: Double?
	let Id: String?
	let address: String?
	let deliveryId: DeliveryDetailId?
	let month: Int?
	let paymentType: String?
	let status: String?
	let deliveryRoomId: String?

	init(_ json: JSON) {
		prescriptionNumber = json["prescriptionNumber"].stringValue
		cancelStatus = json["cancelStatus"].boolValue
		createdAt = json["createdAt"].stringValue
		deliveryType = json["deliveryType"].stringValue
		driverAssignRequest = json["driverAssignRequest"].boolValue
		updatedAt = json["updatedAt"].stringValue
		userId = json["userId"].stringValue
		providerRoomId = json["providerRoomId"].stringValue
		prescriptionImage = json["prescriptionImage"].stringValue
		longitude = json["longitude"].doubleValue
		providerId = ProviderDetailId(json["providerId"])
		prescriptionStatus = json["prescriptionStatus"].stringValue
		year = json["year"].intValue
		_v = json["__v"].intValue
		invoice = json["invoice"].stringValue
		deleteStatus = json["deleteStatus"].boolValue
		offerId = json["offerId"].stringValue
		companyId = json["companyId"].stringValue
		assignDriverStatus = json["assignDriverStatus"].boolValue
		latitude = json["latitude"].doubleValue
		Id = json["_id"].stringValue
		address = json["address"].stringValue
		deliveryId = DeliveryDetailId(json["deliveryId"])
		month = json["month"].intValue
		paymentType = json["paymentType"].stringValue
		status = json["status"].stringValue
		deliveryRoomId = json["deliveryRoomId"].stringValue
	}

}
