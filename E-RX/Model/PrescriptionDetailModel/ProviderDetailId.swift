//
//  ProviderId.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020
//
import Foundation
import SwiftyJSON

struct ProviderDetailId:Equatable {

	let avgRating: Int?
	let mobileNumber: String?
	let Id: String?
	let profilePic: String?
	let fullName: String?
	let countryCode: String?
    let deviceToken: String?
    
	init(_ json: JSON) {
		avgRating = json["avgRating"].intValue
		mobileNumber = json["mobileNumber"].stringValue
		Id = json["_id"].stringValue
		profilePic = json["profilePic"].stringValue
		fullName = json["fullName"].stringValue
		countryCode = json["countryCode"].stringValue
        deviceToken = json["deviceToken"].stringValue
	}

}
