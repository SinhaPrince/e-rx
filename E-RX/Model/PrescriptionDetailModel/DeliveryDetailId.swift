//
//  DeliveryId.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on July 15, 2020
//
import Foundation
import SwiftyJSON

struct DeliveryDetailId :Equatable{

	let deviceToken: String?
	let Id: String?

	init(_ json: JSON) {
		deviceToken = json["deviceToken"].stringValue
		Id = json["_id"].stringValue
	}

}
