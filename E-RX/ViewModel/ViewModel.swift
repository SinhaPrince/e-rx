//
//  ViewModel.swift
//  E-RX
//
//  Created by SinhaAirBook on 24/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

class ViewModel:NSObject {
    
    // Closure use for notifi
    var reloadList = {() -> () in }
    var success = {(message :String) -> () in }
    var errorMessage = {(message : String) -> () in }
    var updateProfile = {() -> () in}
    ///Array of Prescription List Model class
    var prescriptionListArray : [PrescriptionBaseModel] = []{
        ///Reload data when data set
        didSet{
            if (self.prescriptionListArray != oldValue){
                
                self.reloadList()
            }
            
        }willSet(newValue){
            
            self.prescriptionListArray = newValue
        }
    }
    
    let apiHandel : ApiHandler = ApiHandler()
    
    //MARK:- Get data from API for Prescription List
    func getServicecall(URL strUrl:String,Parameters params:[String:Any] , Header header:[String:String]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + strUrl, passDict: params, header: header) { (result) in
            
            switch result{
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.prescriptionListArray.count > 0{
                        self.prescriptionListArray.removeAll()
                    }
                    self.prescriptionListArray.append(PrescriptionBaseModel(json: data))
                    
                    let dateFormatter = DateFormatter()
                          dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"// yyyy-MM-dd"

                    self.prescriptionListArray[0].data = self.prescriptionListArray[0].data.sorted(by: { dateFormatter.date(from:$0.createdAt!)?.compare(dateFormatter.date(from:$1.createdAt!)!) == .orderedDescending })

                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
    }
    
    
    ///Array of Prescription List Model class
    var prescriptionDetailsArray : [PrescriptionDetailBaseModel] = []{
        ///Reload data when data set
        didSet{
            if (self.prescriptionDetailsArray != oldValue){
                
                self.reloadList()
            }
            
        }willSet(newValue){
            
            self.prescriptionDetailsArray = newValue
        }
    }
    
    //MARK:- Get data from API for Prescription Details
    func getPrescriptionDetailServiceCall(URL strUrl:String,Parameters params:[String:Any] , Header header:[String:String]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + strUrl, passDict: params, header: header) { (result) in
            
            switch result{
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.prescriptionDetailsArray.count > 0{
                        self.prescriptionDetailsArray.removeAll()
                    }
                    self.prescriptionDetailsArray.append(PrescriptionDetailBaseModel(data))
                    
                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
    }
    
    //MARK:- Get data from API for Logout Service
    func getLogoutService(){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .get, url: Domain.baseUrl() + APIEndpoint.userLogout, header: header) { (result) in
            
            switch result{
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    let realm = try! Realm()
                    
                    try! realm.write {
                        
                        let deletedNotifications = realm.objects(UserDataModel.self)
                        realm.delete(deletedNotifications)
                        kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window)
                    }
                    
                    
                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
    }
    
    //MARK:- Change Mobile Number Api
    
    func getMobileNumberService(URl url:String,passDict:[String:Any]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + url, passDict: passDict) { (result) in
            
            switch result{
                
            case .success(let data):
                let message = data["message"].string ?? ""
                if data["status"].int == 200{
                    
                    self.reloadList()
                    
                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
    }
    
    func getChangeMobileNumberService(URl url:String,passDict:[String:Any]){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + url, passDict: passDict, header: header) { (result) in
            
            switch result{
                
            case .success(let data):
                let message = data["message"].string ?? ""
                if data["status"].int == 200{
                    
                    self.updateProfile()
                    
                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
    }
    
    //MARK:- Update User Profile Api
    
    var userDataModel = [UserBaseModel]()
    
    func getUpdateProfileService(passDict:[String:Any],imageData:NSData){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchMultipartedApiService(imageData: [imageData], fileName: ["image.png"], imageparams: ["profilePic"], url: Domain.baseUrl() + APIEndpoint.userUpdateDetails, params: passDict, headers: header) { (result) in
            
            switch result {
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.userDataModel.count > 0 {
                        
                        self.userDataModel.removeAll()
                        
                    }
                    self.userDataModel.append(UserBaseModel(json: data))
                    
                    self.success(message)
                }
                else{
                    
                    self.errorMessage(message)
                    
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
        
        
    }
    
    //MARK:- Check Mobile and Email Api
    
    func getCheckMobAndEmailService(passDict : [String:Any]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + APIEndpoint.checkEmailAndMobileAvailability, passDict: passDict){ (result) in
            
            switch result {
                
            case .success(let responseJSON):
                
                let message = responseJSON["message"].string ?? ""
                
                if responseJSON["status"].int == 200{
                    if let data = responseJSON["data"].dictionary{
                        self.otpString = data["otp"]?.stringValue
                        self.success(responseJSON["message"].string ?? "")
                    }
                }
                else{
                    
                    self.errorMessage(message)
                    
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
        
        
        
    }
    
    //MARK:- SignUp Service Api
    
    func getSignupService(URL endUrl:String, passDict:[String:Any],FirstImageData imgData:NSData , FirstFileNamw fileName:String , FirstImageParam imageparam:String,SecondImageData imgData2:NSData , SecondFileNamw fileName2:String , SecondImageParam imageparam2:String ){
        
        apiHandel.fetchMultipartedApiService(imageData: [imgData,imgData2], fileName: [fileName,fileName2], imageparams: [imageparam,imageparam2], url: Domain.baseUrl() + endUrl , params: passDict){ (result) in
            
            switch result {
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.userDataModel.count > 0 {
                        
                        self.userDataModel.removeAll()
                        
                    }
                    self.userDataModel.append(UserBaseModel(json: data))
                    
                    self.success(message)
                }
                else{
                    
                    self.errorMessage(message)
                    
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
        
        
        
    }
    
    
    //MARK:- SignIn Service Api
    
    func getSignInService(passDict:[String:Any]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl() + APIEndpoint.signInApi, passDict: passDict) { (result) in
            
            switch result {
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.userDataModel.count > 0 {
                        
                        self.userDataModel.removeAll()
                        
                    }
                    self.userDataModel.append(UserBaseModel(json: data))
                    
                    self.success(message)
                }
                else{
                    
                    self.errorMessage(message)
                    
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
        
    }
    
    //MARK:- Prescription Request Api
    
    func getPrescriptionReqService(imageData:NSData,lat:String,long:String){
        
        let passDict = ["longitude":lat ,"latitude":long ] as [String:Any]
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchMultipartedApiService(imageData: [imageData], fileName: ["image.png"], imageparams: ["prescriptionImage"], url: Domain.baseUrl() + APIEndpoint.prescriptionRequest, params: passDict, headers: header) { (result) in
            
            switch result {
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if message == "Invalid Token" {
                    
                    self.errorMessage(message)
                    
                    
                }else{
                    
                    self.success(message)
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
        
        
    }
    
    //MARK:- Notification Api Service Calling
    
    var notiDataModel : [NotificationBaseModel] = []{
        didSet{
            if (self.notiDataModel != oldValue){
                self.reloadList()
            }
        }willSet(newValue){
            self.notiDataModel = newValue
        }
    }
    
    
    func getNotificationListService(URL endUrl:String){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .get, url: Domain.baseUrl() + endUrl, header: header) { (result) in
            
            switch result {
                
            case .success(let responseJSON):
                
                if responseJSON["status"].int == 200{
                    
                    if endUrl == "getNotificationList"{
                        
                        if self.notiDataModel.count > 0{
                            
                            self.notiDataModel.removeAll()
                        }
                        self.notiDataModel.append(NotificationBaseModel(json: responseJSON))
                        
                        
                    }else{
                        
                        self.success(responseJSON["message"].string ?? "")
                    }
                    
                    
                }else{
                    
                    self.errorMessage(responseJSON["message"].string ?? "")
                    
                }
                
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
        
        
    }
    
    //MARK:- CheckMobileAvailability Service
    func getcheckMobileAvailabilityService(passDict:[String:Any]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.checkEmailAndMobileAvailability), passDict: passDict) { (result) in
            
            switch result {
            case .success(let responseJSON):
                if responseJSON["status"].int == 200{
                    if let data = responseJSON["data"].dictionary{
                        self.otpString = data["otp"]?.stringValue
                        self.success(responseJSON["message"].string ?? "")
                    }
                }else{
                    self.errorMessage(responseJSON["message"].string ?? "")
                }
                break
            case .failure(let error):
                print(error.localizedDescription)
                self.errorMessage(error.localizedDescription)
            }
            
        }
    }
    
    //MARK:- ChangePassword Service
    
    func getchangePasswordService(passDict : [String:Any]){
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.forgotPassword), passDict: passDict) { (result) in
            
            switch result {
                
            case .success(let responseJSON):
                
                if responseJSON["status"].int == 200{
                    
                    self.success(responseJSON["message"].string ?? "")
                    
                }else{
                    
                    self.errorMessage(responseJSON["message"].string ?? "")
                    
                }
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
            
        }
    }
    
    // My Offers Api Implementation
    
    var myOfferListArr : [PrescriptionOfferList] = []{
        ///Reload data when data set
        didSet{
            if (self.myOfferListArr != oldValue){
                
                self.reloadList()
            }
            
        }willSet(newValue){
            
            self.myOfferListArr = newValue
        }
    }
    
    func getprescriptionOfferListService(){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.prescriptionOfferList), header: header) { (result) in
            
            switch result{
                
            case .success(let data):
                
                let message = data["message"].string ?? ""
                
                if data["status"].int == 200{
                    
                    if self.myOfferListArr.count > 0{
                        self.myOfferListArr.removeAll()
                    }
                    self.myOfferListArr.append(PrescriptionOfferList(data))
                    
                    let dateFormatter = DateFormatter()
                          dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"// yyyy-MM-dd"

                    self.myOfferListArr[0].data = self.myOfferListArr[0].data?.sorted(by: { dateFormatter.date(from:$0.createdAt!)?.compare(dateFormatter.date(from:$1.createdAt!)!) == .orderedDescending })
                    
                    self.reloadList()
                }else{
                    
                    self.errorMessage(message)
                }
                break
            case .failure(let error):
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
    }
    
    //MARK:- Offer Accept and Reject Service
    
    func getPrescriptionRequest(_ url:String,_ passDict:[String:Any]){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(url), passDict: passDict, header: header) { (result) in
            
            switch result {
                
            case .success(let responseJSON):
                
                if responseJSON["status"].int == 200{
                    
                    self.success(responseJSON["message"].string ?? "")
                    
                }else{
                    
                    self.errorMessage(responseJSON["message"].string ?? "")
                    
                }
                break
            case .failure(let error):
                
                print(error.localizedDescription)
                
                self.errorMessage(error.localizedDescription)
            }
            
        }
    }
    
    //MARK:- Notification Status Update
    func getStatusUpdateService(_ url:String,passDict:[String:Any]){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(url), passDict: passDict, header: header) { (result) in
            switch result {
            case .success(let responseJSON):
                if responseJSON["status"].int == 200{
                    self.success(responseJSON["message"].string ?? "")
                }else{
                    self.errorMessage(responseJSON["message"].string ?? "")
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
            
        }
    }
    
    //MARK:- Near By Provider Service
    var nearByProviderArr = [NearByProviderModel]()
    func getNearByProviderService(passDict:[String:Any]){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.nearByProviderList), passDict: passDict, header: header) { (result) in
            switch result {
            case .success(let responseJSON):
                if responseJSON["status"].int == 200{
                    
                    if self.nearByProviderArr.count > 0 {
                        self.nearByProviderArr.removeAll()
                    }
                    self.nearByProviderArr.append(NearByProviderModel(responseJSON))
                    self.reloadList()
                    
                }else{
                    self.errorMessage(responseJSON["message"].string ?? "")
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
        }
    }
    
    //MARK:- Payment Charge Api Service
    
    var chargeDataArr = [ChargeDataModel]()
    
    func getPaymentChargeService(passDict: [String:Any]){
        
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.chargeApi), passDict: passDict, header: header) { (result) in
            switch result{
            case .success(let data):
                if data["code"].int == 501{
                    
                    if self.chargeDataArr.count > 0 { self.chargeDataArr.removeAll() }
                    self.chargeDataArr.append(ChargeDataModel(data))
                    self.success("Charge Run Successfully")
                    
                }else{
                    self.errorMessage(data["message"].string ?? "")
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
        }
    }
    
    //MARK:- paymentApi Service(Add Payment)
    
    func getpaymentApiSrvice(passDict:[String:Any]){
       
        let realm = try! Realm()
        let scope = realm.objects(UserDataModel.self).first
        let header = ["Authorization":scope?.Token] as! [String:String]
        
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.paymentApi), passDict: passDict, header: header) { (result) in
            switch result{
            case .success(let data):
                print(data)
                self.success("Payment Successfully")
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
        }
    }
    
    //MARK:- checkEmailForForgotPassword
    
    var otpString: String?
    
    func getcheckEmailForForgotPassword(_ email: String){
        apiHandel.fetchApiService(method: .post, url: Domain.baseUrl().appending(APIEndpoint.checkEmailForForgotPassword), passDict: ["email" : "email"]) { (result) in
            switch result{
            case .success(let responseJSON):
                if responseJSON["status"].int == 200{
                    if let data = responseJSON["data"].dictionary{
                        self.otpString = data["otp"]?.stringValue
                        self.success(responseJSON["message"].string ?? "")
                    }
                }else{
                    self.errorMessage(responseJSON["message"].string ?? "")
                }
                break
            case .failure(let error):
                self.errorMessage(error.localizedDescription)
            }
        }
    }
}
