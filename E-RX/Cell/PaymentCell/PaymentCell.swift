//
//  PaymentCell.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var lbl_BankName: UILabel!
    @IBOutlet weak var lbl_CardNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
