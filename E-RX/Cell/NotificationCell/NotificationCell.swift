//
//  NotificationCell.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    //MARK:- Outlet
    
    @IBOutlet weak var lbl_Message: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Set Data On Cell
    
    func setData(data:NotificationData){
        
        comClass.LblFontSizeName(name: "\(data.notiTitle!) \n\(data.notiMessage!)", lbl: lbl_Message, textColor: comClass.textDarkGray, fontName: comClass.RegularFont, fontSize: 16)
        comClass.LblFontSizeName(name: DateConverter(date: String(data.createdAt!.prefix(19))), lbl: lbl_Time, textColor: comClass.textLightGray, fontName: comClass.RegularFont, fontSize: 14)
        
    }
    
    func DateConverter(date:String) -> String {
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        var date = dateFormatter.date(from: date)// create   date from string
        var timeR = String()
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let currentDate = Date()
        
        date = dateFormatter.date(from: dateFormatter.string(from: date!))!
        
        let diff = Int(currentDate.timeIntervalSince1970 - date!.timeIntervalSince1970)
        let Day = diff / (3600 * 24)
        let Hour = diff / 3600
        let minutes = (diff - Hour * 3600) / 60
        //let seconds = minutes * 60
        
        if (Day >= 1){
            
            timeR = String(Day) + " days ago"
            
        }else if (Hour >= 1){
            
            timeR = String(Hour) + " hours ago"
        }else if minutes >= 1{
            
            timeR = String(minutes) + " mins ago"
        }else{
            
            timeR = "now"
        }
        
        
        return timeR
        
    }
    
}
