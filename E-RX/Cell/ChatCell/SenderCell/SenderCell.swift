//
//  SenderCell.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class SenderCell: UITableViewCell {
    
    //MARK:- Outlet
    
    @IBOutlet weak var lbl_Sender: UILabel!
    @IBOutlet weak var lbl_Timw: UILabel!
    
    var item: ChatData? {
        didSet {
            guard let item = item else {
                return
            }
            comClass.LblFontSizeName(name: item.message ?? "", lbl: lbl_Sender, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
            comClass.LblFontSizeName(name: comClass.timeConversion(createdAt: item.createdAt ?? ""), lbl: lbl_Timw, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 12)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
