//
//  RegisteredProvoderCell.swift
//  E-RX
//
//  Created by SinhaAirBook on 21/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class RegisteredProvoderCell: UITableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var lbl_FullName: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Mobile: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    var item: NearByProviderData? {
        didSet {
            guard let item = item else {
                return
            }
            comClass.LblFontSizeName(name: item.fullName ?? "", lbl: lbl_FullName, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
            comClass.LblFontSizeName(name: item.address ?? "", lbl: lbl_Address, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
            comClass.LblFontSizeName(name: item.email ?? "", lbl: lbl_Email, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
            comClass.LblFontSizeName(name: item.mobileNumber ?? "", lbl: lbl_Mobile, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
            profilePic.setImage(withImageId: item.profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
