//
//  PrescriptionCell.swift
//  E-RX
//
//  Created by macbook on 28/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class PrescriptionCell: UITableViewCell {

    
    //MARK:- Outlet
    
    @IBOutlet weak var lbl_BookingIDHeader: UILabel!
    @IBOutlet weak var lbl_BookingDetail: UILabel!
    @IBOutlet weak var lbl_BookingDate: UILabel!
    @IBOutlet weak var lbl_DeliveryDate: UILabel!
    @IBOutlet weak var lbl_Type: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    func setDataOnCell(data:PriscriptionData,type:Int){
        
        let strMul = NSMutableAttributedString()
        strMul.append(comClass.attributedText(string: type == 1 ?
            comClass.createString(Str: "Booking ID")  : comClass.createString(Str: "Prescription ID") , font: comClass.RegularFont , color: AppColor.textColor, size: 14))
        strMul.append(comClass.attributedText(string:"  \(data.prescriptionNumber ?? "")", font: comClass.BoldFont , color: AppColor.textColor, size: 16))
        
        lbl_BookingIDHeader.attributedText = strMul
       
        comClass.LblFontSizeName(name: type == 1 ? comClass.createString(Str: "Booking Detail") : comClass.createString(Str: "Prescription Detail"), lbl: lbl_BookingDetail, textColor: comClass.textLightGray, fontName: comClass.RegularFont, fontSize: 14)
        comClass.LblFontSizeName(name:"Status: " + (data.prescriptionStatus ?? ""), lbl: lbl_Status, textColor: AppColor.textColor, fontName:comClass.RegularFont, fontSize: 14)
        comClass.LblFontSizeName(name: "Booking Date: " + (comClass.dateTimeConversion(createdAt: data.createdAt ?? "")), lbl: lbl_BookingDate, textColor: AppColor.textColor, fontName:comClass.RegularFont, fontSize: 14)
        comClass.LblFontSizeName(name: "Type: " + (data.deliveryType ?? ""), lbl: lbl_Type, textColor: AppColor.textColor, fontName:comClass.RegularFont, fontSize: 14)
        comClass.LblFontSizeName(name: "Address: " + (data.address ?? ""), lbl: lbl_Address, textColor: AppColor.textColor, fontName:comClass.RegularFont, fontSize: 14)
        comClass.LblFontSizeName(name: type == 1 ? "" : ("Delivery Date: " + (comClass.dateTimeConversion(createdAt: data.updatedAt ?? ""))), lbl: lbl_DeliveryDate, textColor: AppColor.textColor, fontName:comClass.RegularFont, fontSize: 14)
    }
    
}
