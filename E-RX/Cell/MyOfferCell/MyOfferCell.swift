//
//  MyOfferCell.swift
//  E-RX
//
//  Created by SinhaAirBook on 10/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class MyOfferCell: UITableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var lbl_HeaderReq: UILabel!
    @IBOutlet weak var img_View: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var btn_Rate: UIButton!
    @IBOutlet weak var lbl_Amount: UILabel!
    @IBOutlet weak var lbl_TotalAmount: UILabel!
    @IBOutlet weak var btn_Accept: UIButton!
    @IBOutlet weak var btn_Reject: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initializeUi()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func initializeUi(){
        
        comClass.LblFontSizeName(name: "Request", lbl: lbl_HeaderReq, textColor: AppColor.textColor, fontName: comClass.BoldFont, fontSize: 17)
        comClass.LblFontSizeName(name: "Total Payment", lbl: lbl_TotalAmount, textColor: AppColor.placeHolderColor, fontName: comClass.RegularFont, fontSize: 12)
        comClass.setDataOnButton(btn: btn_Accept, text: "Accept", font: comClass.RegularFont, size: 16, textcolor: UIColor.white, image: UIImage(), backGroundColor: .clear, aliment: .center)
        comClass.setDataOnButton(btn: btn_Reject, text: "Reject", font: comClass.RegularFont, size: 16, textcolor: #colorLiteral(red: 0.4900352359, green: 0.09707603604, blue: 0.08556544036, alpha: 0.9776862158), image: UIImage(), backGroundColor: .clear, aliment: .center)
        
    }
    
    func setDataOnScreen(_ data:DataModel){
        
        comClass.LblFontSizeName(name: data.providerData?.fullName ?? "", lbl: lbl_Name, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
        comClass.setDataOnButton(btn: btn_Rate, text: " \((data.providerData?.avgRating)!)", font: comClass.RegularFont, size: 16, textcolor: AppColor.textColor, image: UIImage(named: "star")!, backGroundColor: UIColor.clear, aliment: .left)
        img_View.setImage(withImageId: data.providerData?.profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        
        let str = NSMutableAttributedString(string: "\(data.currency ?? "INR") ", attributes: [NSAttributedString.Key.font :  AppFont.Regular.size(AppFontName.Lato, size: 13),NSAttributedString.Key.foregroundColor:AppColor.textColor])
        str.append(NSAttributedString(string: String(data.amount ?? 0), attributes: [NSAttributedString.Key.font :  AppFont.Regular.size(AppFontName.Lato, size: 19),NSAttributedString.Key.foregroundColor:AppColor.appColor]))
        lbl_Amount.attributedText = str
        
    }
    
    
}
