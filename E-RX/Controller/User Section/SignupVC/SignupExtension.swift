//
//  SignupExtension.swift
//  E-RX
//
//  Created by SinhaAirBook on 12/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import UIKit

extension SignUpVC{
    
    func validation(){
        
        var message = ""
        
        if self.txt_FullName.text!.isEmpty{

            message = "Please enter your Full Name."
            
        }else if self.txt_Mobile.text!.isEmpty{
            
            message = "Please enter your Mobile Number."
            
        }else if self.txt_EmailId.text!.isEmpty{
            
            message = "Please enter your Email Id."
            
        }else if self.txt_EmiratesId.text!.isEmpty {
            
            message = "Please enter your Emirates Id."
            
        }else if self.emiratesIdPic.isEmpty{
            
            message = "Please choose your Emirates Id Photo."
            
        }
//        else if !self.btn_CamInsurrance.isSelected && self.insuranceCardPic.isEmpty {
//
//
//
//                message = "Please choose your Insurance Card Id Photo."
//
//
//
//        }
        else if self.txt_DOB.text!.isEmpty{
            
            message = "Please enter your Date of Birth."
            
        }else if self.isGenderSelect == false{
            
            message = "Please select your Gender."
            
        }else if self.txt_SelectLoc.text!.isEmpty || self.txtView_Address.text!.isEmpty{
            
            message = "Please enter your Address."
            
        }else if self.txt_CreatePass.text!.isEmpty{
            
            message = "Please enter your Password."
            
        }else if self.txt_ConfirmPass.text != self.txt_CreatePass.text{
            
            message = "Your password did't match."
            
        }else if !self.btn_AccpetTC.isSelected{
            
            message = "Please select our Terms and Conditions."
            
        }
        
        message == "" ? self.gotoOTPView() : super.handleError(message)
    }
  
}

//MARK:- SelectAddressVCDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate

extension SignUpVC:backToMain,passAddress,OTPAuthenticationDelegate{
    
    
    func otpAuthCompletion() {
     
        let address = (self.txt_SelectLoc.text ?? "") != "" ? self.txt_SelectLoc.text : self.txtView_Address.text ?? ""
        
        let DToken = UserDefaults.standard.value(forKey: "DeviceToken") as? String ?? ""
        
        var passDict =  [ "fullName":self.txt_FullName.text ?? "",
                        "email":self.txt_EmailId.text ?? "",
                        "countryCode":self.txt_countryCode.text ?? "",
                        "mobileNumber":self.txt_Mobile.text ?? "",
                        "address":address ?? "" ,
                        "password":self.txt_CreatePass.text ?? "",
                        "deviceType":"ios",
                        "deviceToken":"Web",
                        "latitude":self.lat ?? "",
                        "longitude":self.long ?? "","deviceType":"iOS","deviceToken":DToken] as [String:Any]
        
        passDict["emiratesId"] = self.txt_EmiratesId.text ?? ""
        passDict["gender"] = self.gender ?? ""
        passDict["dob"] = self.txt_DOB.text ?? ""
        
        print(passDict)
        
        showActivityIndicator(view: view)
        
        self.viewModel.getSignupService(URL: APIEndpoint.userSignup, passDict: passDict, FirstImageData: self.emiratesIdPic, FirstFileNamw: "image.png", FirstImageParam: "emiratesIdPicture", SecondImageData: self.insuranceCardPic, SecondFileNamw: "image.png", SecondImageParam: "insuranceCardPicture")
        self.closureSetUp()
        
    }
    
    func backAddressToVC(Address:String,addressType:String,lat:String,long:String){
        
        self.txt_SelectLoc.text = Address
        self.lat = lat
        self.long = long
        self.txtView_Address.isUserInteractionEnabled = false
        
    }
    
    
    func back() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddVC") as! SearchAddVC
       if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        vc.obj = self
        self.present(vc, animated: true, completion: nil)
    }
    
   
    
    
    func setAttributedPlaceHolder(str:[String],textFld:[UITextField]){
        
        for i in 0..<textFld.count{
            
            let passwordAttriburedString = NSMutableAttributedString(string: comClass.createString(Str: str[i]) )
            let asterix = NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red])
            passwordAttriburedString.append(asterix)
            textFld[i].font = AppFont.Regular.size(AppFontName.Lato, size: 17)
            textFld[i].textColor = AppColor.textColor
            textFld[i].attributedPlaceholder = passwordAttriburedString
            textFld[i].textAlignment = Localize.currentLanguage() == "en" ? .left : .right
        }
    
    }
    
}

extension SignUpVC:UINavigationControllerDelegate,UIImagePickerControllerDelegate,cameraAndgalleryDelegate{
    
    
    func camAndGallery(type: String) {
        
        type == "Camera" ? comClass.openCamera(imagePicker: imagePicker, vc: self):comClass.openGallery(imagePicker: imagePicker, vc: self)
        
    }
    
    // MARK:- IMAGEPICKER DELEGATE
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            if let imageData = chosenImage.jpegData(compressionQuality: 1.0) as NSData? {
                
                if self.idPicSelected == "emirates"{
                   
                    self.emiratesIdPic = imageData
                   
                    self.btn_EID.setImage(chosenImage, for: .normal)
                    
                }else{
                    
                    self.insuranceCardPic = imageData
                    self.btn_CamInsurrance.setImage(chosenImage, for: .normal)
                }
            
                
            }else{
                print("imageData nahi aaya gya")
            }
        } else{
            print("Something went wrong")
        }
       // self.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:- DatePicker
    //MARK:-
    
    func DatePicker(sender:UIButton,txtFld:UITextField){
        
        self.view.addSubview(baseview)
        self.view.endEditing(true)
        
        let doneButton : UIButton = {
            
            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
            btn.tag = 13
            btn.addTarget(self, action: #selector(btnClick(sender:)), for: .touchUpInside)
            
            return btn
        }()
        
        let cancelButton : UIButton = {
            
            let btn = UIButton(frame: CGRect(x: baseview.frame.size.width-100, y: 0, width: 100, height: 50))
            btn.tag = 14
            btn.addTarget(self, action: #selector(btnClick(sender:)), for: .touchUpInside)
            
            return btn
        }()
        view.addSubview(baseview)
        _ = baseview.openDatePicker(sender: sender.tag, baseview: baseview, doneButton: doneButton, cancelButton: cancelButton, txtDate: txtFld)
        
        
    }
    
}

// An attributed string extension to achieve colors on text.
extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}

