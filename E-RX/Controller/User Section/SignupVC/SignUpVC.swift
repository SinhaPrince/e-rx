//
//  SignUpVC.swift
//  E-RX
//
//  Created by macbook on 30/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import ADCountryPicker
import RealmSwift

class SignUpVC: BaseViewController ,UITextViewDelegate{
    
    //MARK:- Outlet
    @IBOutlet weak var txt_FullName : UITextField!
    @IBOutlet weak var txt_countryCode : UITextField!
    @IBOutlet weak var txt_Mobile : UITextField!
    @IBOutlet weak var txt_EmailId : UITextField!
    @IBOutlet weak var txt_EmiratesId : UITextField!
    @IBOutlet weak var lbl_InsuranceCard: UILabel!
    @IBOutlet weak var lbl_NotAvaiable: UILabel!
    @IBOutlet weak var txt_DOB: UITextField!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var btn_Female: UIButton!
    @IBOutlet weak var btn_Male: UIButton!
    @IBOutlet weak var txt_SelectLoc: UITextField!
    @IBOutlet weak var lbl_OR: UILabel!
    @IBOutlet weak var txtView_Address: UITextView!
    @IBOutlet weak var txt_AddMobileNum: UITextField!
    @IBOutlet weak var txt_CreatePass: UITextField!
    @IBOutlet weak var txt_ConfirmPass: UITextField!
    @IBOutlet weak var lbl_acceptTermsCond: UILabel!
    @IBOutlet weak var btn_SignUp: UIButton!
    @IBOutlet weak var btn_CamInsurrance: UIButton!
    @IBOutlet weak var btn_AccpetTC : UIButton!
    @IBOutlet weak var btn_EID: UIButton!
    @IBOutlet weak var btn_SelectLoc : UIButton!
    @IBOutlet weak var viewScroll: UIView!
     @IBOutlet weak var btnClose: UIButton!
    //MARK:- Variables
    
    let obj = CommonClass.sharedInstance
    var imagePicker = UIImagePickerController()
    var insuranceCardPic = NSData()
    var emiratesIdPic = NSData()
    var idPicSelected = String()
    var isGenderSelect : Bool?
    var lat : String?
    var long : String?
    var gender : String?
    var nav:UINavigationController?
    
    lazy var baseview:UIView = {
        
        let view =  UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-350, width: self.view.frame.size.width, height: 350))
        view.backgroundColor = CommonClass.sharedInstance.AppColorMost
        view.tag = 668
        
        return view
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.txtViewDelegate()
        configureElements()
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        
        addSwipGesture(view: viewScroll)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar(true)
        super.viewDidAppear(animated)
        
        if #available(iOS 13.0, *) {
            self.btnClose.isHidden = true
        }else{
            self.btnClose.isHidden = false
            self.btnClose.addTarget(self, action: #selector(btnCloseTap), for: .touchUpInside)
        }
        
        
    }
    
    //MARK:- Configure and Set UIName on UIElements
    //MARK:-
    
    private func configureElements(){
        
        addDoneButtonOnKeyboard(txtFld: [txt_FullName,txt_Mobile,txt_EmailId,txt_CreatePass,txt_EmiratesId,txt_ConfirmPass,txt_AddMobileNum])
        
        configureView(txtFld: [txt_FullName,txt_countryCode,txt_DOB,txt_Mobile,txt_EmailId,txt_SelectLoc,txt_CreatePass,txt_EmiratesId,txt_ConfirmPass,txt_AddMobileNum])
        
        txtView_Address.contentInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        
        self.setAttributedPlaceHolder(str: ["Full Name","DOB","Mobile Number","Select Location","Create Password","Confirm Password","Emirates Id Number"], textFld: [txt_FullName,txt_DOB,txt_Mobile,txt_SelectLoc,txt_CreatePass,txt_ConfirmPass,txt_EmiratesId])
        self.txt_countryCode.text = "+91"
        txtFldFontSizeConfig(txtFld: [txt_countryCode,txt_EmailId,txt_AddMobileNum], placeHolderText: [.countryCode,.email,.AddNalMobNo])
        
        LblFontSizeName(name: [.NotAvialable,.Gender,.ATC], lbl: [lbl_NotAvaiable,lbl_Gender,lbl_acceptTermsCond])
        
        obj.LblFontSizeName(name: "Insurance Card Picture (Fornt Side)", lbl: lbl_InsuranceCard, textColor: UIColor.lightGray, fontName: "Lato-Italic", fontSize: 15)
        
        obj.LblFontSizeName(name: "OR", lbl: lbl_OR, textColor: UIColor.lightGray, fontName: obj.RegularFont, fontSize: 15)
        
        obj.setDataOnButton(btn: btn_Male, text: "  \(comClass.createString(Str: "Male"))", font: obj.RegularFont, size: obj.BoldfontSize, textcolor: UIColor.lightGray, image: #imageLiteral(resourceName: "radio_unc"), backGroundColor: .clear, aliment: .center)
        
        obj.setDataOnButton(btn: btn_Female, text: "  \(comClass.createString(Str: "Female"))", font: obj.RegularFont, size: obj.BoldfontSize, textcolor: UIColor.lightGray, image: #imageLiteral(resourceName: "radio_un"), backGroundColor: .clear, aliment: .center)
        
        obj.setDataOnButton(btn: btn_SignUp, text: "Sign Up", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: .white, image: UIImage(), backGroundColor: obj.AppColorMost, aliment: .center)
    }
    
    
    //MARK:- UIButton Action
    
    //MARK:- SignUp Btn
    
    @IBAction func btn_SignUP(_ sender: UIButton) {
        
        self.validation()
        
    }
    
    @objc func btnCloseTap(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Camera Btn
    
    
    @IBAction func btn_CamTap(_ sender: UIButton) {
        
        // let vc = storyboard?.instantiateViewController(withIdentifier: "AllowCamPopVC") as! AllowCamPopVC
        // self.present(vc, animated: false, completion: nil)
        
    }
    
    //MARK:- Select Location Btn
    
    @IBAction func btn_SelectLocTap(_ sender: UIButton) {
        
        let bool = UserDefaults.standard.value(forKey: "isLocationAllow") as? Bool ?? false
        
        if bool == true{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddVC") as! SearchAddVC
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            vc.obj = self
            self.present(vc, animated: true, completion: nil)
            
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LocationPopUpVC") as! LocationPopUpVC
            vc.delegate = self
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            vc.nav = self.navigationController ?? UINavigationController()
            self.present(vc, animated: false, completion: nil)
            
        }
        
        
    }
    
    
    @IBAction func btnTapAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let picker = ADCountryPicker()
            let pickerNavigationController = UINavigationController(rootViewController: picker)
            self.present(pickerNavigationController, animated: true, completion: nil)
            picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
                print(dialCode)
                self.txt_countryCode.text = dialCode
                self.dismiss(animated: false, completion: nil)
            }
        } else if sender.tag == 2 || sender.tag == 3{
            
            self.idPicSelected = sender.tag == 2 ? "emirates" : "insurrance"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeCameraPopUpVC") as! HomeCameraPopUpVC
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            vc.delegateCamAndGall = self
            self.present(vc, animated: false, completion: nil)
            
        }else if sender.tag == 4{
            
            print(sender.isSelected)
            sender.setImage(((sender.isSelected == false) ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "radio_un")), for: .normal)
            
            if sender.isSelected == true{
                
                btn_CamInsurrance.isUserInteractionEnabled = true
                btn_CamInsurrance.isHidden = false
                
            }else{
                
                btn_CamInsurrance.isUserInteractionEnabled = false
                btn_CamInsurrance.isHidden = true
                self.insuranceCardPic = NSData()
                btn_CamInsurrance.setImage(#imageLiteral(resourceName: "cam_white"), for: .normal)
                
            }
            
            sender.isSelected = !sender.isSelected
            
        }else if sender.tag == 5 {
            
            self.DatePicker(sender: sender, txtFld: self.txt_DOB)
            
        }else if sender.tag == 6{
            
            sender.setImage(#imageLiteral(resourceName: "radio_c"), for: .normal)
            sender.setTitleColor(obj.AppColorMost, for: .normal)
            
            btn_Female.setImage(#imageLiteral(resourceName: "radio_unc"), for: .normal)
            btn_Female.setTitleColor(UIColor.lightGray, for: .normal)
            self.isGenderSelect = true
            self.gender = "Male"
            
        }else if sender.tag == 7{
            
            sender.setImage(#imageLiteral(resourceName: "radio_c"), for: .normal)
            sender.setTitleColor(obj.AppColorMost, for: .normal)
            
            btn_Male.setImage(#imageLiteral(resourceName: "radio_unc"), for: .normal)
            btn_Male.setTitleColor(UIColor.lightGray, for: .normal)
            self.isGenderSelect = true
            self.gender = "Female"
            
        }else if sender.tag == 8{
            
            self.txt_CreatePass.isSecureTextEntry = !self.txt_CreatePass.isSecureTextEntry
        }else if sender.tag == 9{
            
            self.txt_ConfirmPass.isSecureTextEntry = !self.txt_ConfirmPass.isSecureTextEntry
        }else if sender.tag == 10{
            
            print(sender.isSelected)
            sender.setImage(((sender.isSelected == false) ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "radio_un")), for: .normal)
            sender.isSelected = !sender.isSelected
            
        }
        
    }
    
    
    //MARK:- TxtView Custom Method..
    fileprivate func txtViewDelegate(){
        
        txtView_Address.textColor = .lightGray
        txtView_Address.text = comClass.createString(Str: "Enter your address")
        txtView_Address.delegate = self
    }
    
    //MARK:- TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        
        if (txtView_Address.text == comClass.createString(Str: "Enter your address"))
            
        {
            txtView_Address.text = nil
            txtView_Address.textColor = obj.textDarkGray
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        
        if txtView_Address.text.isEmpty
        {
            txtView_Address.text = comClass.createString(Str: "Enter your address")
            txtView_Address.textColor = .lightGray
            self.btn_SelectLoc.isUserInteractionEnabled = true
            self.txt_SelectLoc.isUserInteractionEnabled = true
        }else{
            
            self.btn_SelectLoc.isUserInteractionEnabled = false
            self.txt_SelectLoc.isUserInteractionEnabled = false
        }
        txtView_Address.resignFirstResponder()
    }
    
    
    //MARK:- DatePicker
    @objc func btnClick(sender:UIButton){
        
        if sender.tag == 13{
            
            
            if let baseViewTag = self.view.viewWithTag(668)
            {
                if let datePicker = self.view.viewWithTag(5454) as? UIDatePicker
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat =  "yyyy-MM-dd"
                    
                    let date = dateFormatter.string(from: datePicker.date)
                    
                    self.txt_DOB.text = date
                    
                    baseViewTag.removeFromSuperview()
                    datePicker.removeFromSuperview()
                }
            }
        }else if sender.tag == 14{
            
            if let baseViewTag = self.view.viewWithTag(668)
            {
                baseViewTag.removeFromSuperview()
            }
            
        }
        
    }
    
    
  
    
    func gotoOTPView(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationVc") as! OtpVerificationVc
        vc.dataItem = passOTPData(email: self.txt_EmailId.text ?? "", mobile: self.txt_Mobile.text ?? "", countryCode: self.txt_countryCode.text ?? "", comingFrom: "SignUp")
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = {[weak self] (message) in
            
            DispatchQueue.main.async {
                
                let realm = try! Realm()
                let scope = realm.objects(WalkThorughShow.self)
                
                if scope.first?.isAppStarted == "Yes"{
                    kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
                }else{
                    self?.moveToNextViewC(name: "Main", withIdentifier: "WalkThrough")
                }
                self?.handleError(message)
            }
            
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
    }
    
    
}

