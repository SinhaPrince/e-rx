//
//  OtpVerificationVc.swift
//  E-RX
//
//  Created by CST on 5/28/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import FirebaseAuth

class OtpVerificationVc: BaseViewController {
    
    
    //MARK:- Outlet
    @IBOutlet weak var OtpVieww: VPMOTPView!
    @IBOutlet weak var lbl_EnterOTPHeader: UILabel!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var btn_Resend: UILabel!
    @IBOutlet weak var TopView: UIView!
    
    //MARK:- Variables
    var comingFrom = String()
    var delegate : OTPAuthenticationDelegate?
    var verifyID : String?
    var isOtpCorrect : String?
    var phoneNumber:String?
    var mobile:String?
    var dataItem: passOTPData?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        //self.firebaseAuth()
        self.pageSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configUIElements()
        defaultConfiguration()
        
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        addBlurOnView(view: TopView)
    }
    
    //MARK:- Configure UIComponents
    
    private func configUIElements(){
        
        LblFontSizeName(name: [.resendOtp], lbl: [btn_Resend])
        setDataOnButton(btn: [btn_Submit], text: [.submit], textcolor: [.white])
        comClass.LblFontSizeName(name: "Enter the OTP received on email starting with \(mobile?.prefix(6) ?? "")************", lbl: lbl_EnterOTPHeader, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 16)
    }
    
    
    //MARK:- UIButton Action
    
  
    
    //MARK:- Btn Submit
    
    @IBAction func btn_Submit(_ sender: UIButton) {
       
        if  self.isOtpCorrect == self.viewModel.otpString  {
            self.dismiss(animated: true) {
                self.delegate?.otpAuthCompletion()
            }
        }else{
            self.handleError("Please enter correct OTP")
        }
        
    }
    
    @IBAction func btn_Resend(_ sender:UIButton){
      //  self.firebaseAuth()
        self.pageSetup()
    }
}

//MARK:- Firebase Setup
//MARK:-

extension OtpVerificationVc{
    
    func firebaseAuth(){
        
        PhoneAuthProvider.provider().verifyPhoneNumber(self.phoneNumber ?? "", uiDelegate: nil) { (verificationID, error) in
            
            guard error == nil else {
                self.handleError(error?.localizedDescription)
                return
            }
            self.handleError("OTP has been sent to your Mobile Number.")
            self.verifyID = verificationID
        }
        
    }
    
    internal func pageSetup(){
        
        showActivityIndicator(view: view)
        DispatchQueue.main.async {
            if self.dataItem?.comingFrom == "SignUp"{
                let passDict = ["countryCode":self.dataItem?.countryCode ?? "",
                                "mobileNumber":self.dataItem?.mobile ?? "",
                                "email":self.dataItem?.email ?? ""]  as [String:Any]
                self.viewModel.getcheckMobileAvailabilityService(passDict: passDict)
            }else{
                self.viewModel.getcheckEmailForForgotPassword(self.dataItem?.mobile ?? "")
            }
            self.closerSetup()
        }
    }
    
    internal func closerSetup(){
        
        viewModel.success = {[weak self] (message) in
            DispatchQueue.main.async {
                self?.handleError(message)
                self?.initializeOtpView(self?.viewModel.otpString?.count ?? 0)
            }
        }
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                self?.handleError(message)
            }
        }
    }
}

//MARK:- VPMOTPViewDelegate
//MARK:
extension OtpVerificationVc: VPMOTPViewDelegate{
    
    func initializeOtpView(_ count: Int){
        
        self.hideNavigationBar(true)
        //..........OTP..........
        
        self.OtpVieww.otpFieldDefaultBorderColor = AppColor.appColor
        self.OtpVieww.otpFieldEnteredBorderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.OtpVieww.otpFieldErrorBorderColor = UIColor.red
        self.OtpVieww.otpFieldBorderWidth = 1
        self.OtpVieww.otpFieldsCount = count
        self.OtpVieww.delegate = self
        self.OtpVieww.shouldAllowIntermediateEditing = false
        self.OtpVieww.initializeUI()
        
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        
        return self.isOtpCorrect == viewModel.otpString
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        
        self.isOtpCorrect = otpString
        
    }
    
    func chekingForOTPConfirmation(verificationCode:String)
    {
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verifyID ?? "", verificationCode: verificationCode)
        
        Auth.auth().signIn(with: credential) { (user , error) in
            
            self.hideActivityIndicator()
            
            guard error == nil else {
                
                self.handleError(error?.localizedDescription)
                
                return
            }
             self.dismiss(animated: true) {
                 self.delegate?.otpAuthCompletion()
             }
          
        }
      
    }
}

struct passOTPData {
    var email: String
    var mobile: String
    var countryCode: String
    var comingFrom: String
}
