//
//  ForgotPasswordVc.swift
//  E-RX
//
//  Created by CST on 5/28/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import ADCountryPicker

class ForgotPasswordVc: BaseViewController {

    //MARK:- Outlet
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var lbl_PleaseEnterHeader: UILabel!
    @IBOutlet weak var btn_SendOTP: UIButton!
    @IBOutlet weak var btn_CountryCode : UIButton!
    
    //MARK:- Variables
    var userId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        initialSetup()
    }
    
    //MARK:- setUpUI Elements
    func initialSetup(){
        
        configureView(txtFld: [txtPhoneNumber])
        LblFontSizeName(name: [.PleaseEnterRegMNO], lbl: [lbl_PleaseEnterHeader])
       
        txtFldFontSizeConfig(txtFld: [txtPhoneNumber], placeHolderText: [.phoneNo])
        setDataOnButton(btn: [btn_SendOTP,btn_CountryCode], text: [.sendOtp,.countryCode], textcolor: [AppColor.whiteColor,AppColor.textColor])
        setNavi()
    }
    
    //MARK:- SetUp Navigation
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("Forgot Password", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
    
    @IBAction func btnbackTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
   
    @IBAction func btnSendOtpTap(_ sender: Any) {
        
        pageSetup()
       
    }
    
    @IBAction func btnCountryCode(_ sender: UIButton){
        
        let picker = ADCountryPicker()
        let pickerNavigationController = UINavigationController(rootViewController: picker)
        self.present(pickerNavigationController, animated: true, completion: nil)
        picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
            print(dialCode)
            self.btn_CountryCode.setTitle(dialCode, for: .normal)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
}

//MARK:- Api Service Calling
//MARK:-

extension ForgotPasswordVc:OTPAuthenticationDelegate{
    
    
    // Initial page settings
    internal func pageSetup()  {
        
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            
            let passDict = ["countryCode":self.btn_CountryCode.titleLabel?.text ?? "","mobileNumber":self.txtPhoneNumber.text ?? ""] as [String:Any]
            
            self.viewModel.getcheckMobileAvailabilityService(passDict:passDict)
            self.closureSetUp()
            
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = {[weak self] (userId) in
            
            self?.userId = userId // It is used in Forgot password api
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationVc") as! OtpVerificationVc
                vc.phoneNumber = (self?.btn_CountryCode.titleLabel?.text)! + (self?.txtPhoneNumber.text)!
                vc.delegate = self
                vc.mobile = self?.txtPhoneNumber.text
                if #available(iOS 13.0, *) {
                    vc.modalPresentationStyle = .formSheet
                }
                self?.present(vc, animated: true, completion: nil)
                
            }
            
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
    }
    
    func otpAuthCompletion() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordPopUpVC") as! ResetPasswordPopUpVC
        vc.userID = userId
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        self.navigationController?.present(vc, animated: false, completion: nil)
    }
    
    
}
