//
//  PaymentVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btn_AddCard: UIButton!
    @IBOutlet var footerView: UIView!
    
    //MARK:- Varibales
    
    let obj = CommonClass.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        configTableView()
        
        obj.setDataOnButton(btn: btn_AddCard, text: "Add Card", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: .white, image: UIImage(), backGroundColor: UIColor(), aliment: UIControl.ContentHorizontalAlignment.center)
        
    }
    
    //MARK:- UIButton Action
    
    //MARK:- Btn Add Card
    
    @IBAction func btn_addCard(_ sender: UIButton) {
        
        let vc  = storyboard?.instantiateViewController(withIdentifier: "AddCardVC") as! AddCardVC
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
   
}

//MARK:- UITableView Delegate and DataSource

extension PaymentVC:UITableViewDelegate,UITableViewDataSource{
    
    
    //MARK:- configTableView
    
    private func configTableView(){
        
        tableView.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = footerView
        
    }
    
    //MARK:- numberOfRowsInSection
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
}
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        obj.LblFontSizeName(name: "Kodak Mahindra Bank", lbl: cell.lbl_BankName, textColor: .black, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        obj.LblFontSizeName(name: "**** **** **** 1234", lbl: cell.lbl_CardNumber, textColor:.black, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        return cell
    }
    
}
