//
//  MyProfileVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class MyProfileVC: BaseViewController {

    //MARK:- Outlet
    @IBOutlet weak var btn_Profile: UIButton!
    @IBOutlet weak var btn_Payment: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    
    //MARK:- Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        intialSetUp()
        defaultConfiguration()
        setName()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setNavi()
    }
    
    //MARK:- Navigation Setup
    func setNavi(){
        
        super.delagateSearch = self
        hideNavigationBar(false)
        setupNavigationBarTitle("My Profile", leftBarButtonsType: [.back], rightBarButtonsType: [.logout])
        self.baseDelegate?.navigationBarButtonDidTapped(.logout)
        
    }
    
    //MARK:- Set UIName on UIElements
    //MARK:-
    
    private func setName(){
        
        setDataOnButton(btn: [btn_Profile,btn_Payment], text: [.Profile,.Payment], textcolor: [AppColor.whiteColor,AppColor.textColor])
        
        
    }
    
    //MARK:- Btn Action
    
    //MARK:- On Going
    
    @IBAction func btnProfile(_ sender: UIButton) {
        
        sender.backgroundColor = AppColor.appColor
        sender.setTitleColor(.white, for: .normal)
        btn_Payment.backgroundColor = .clear
        btn_Payment.setTitleColor(AppColor.textColor, for: .normal)
        
        animateScrollViewHorizontally(destinationPoint: CGPoint(x: 0 * self.view.frame.width, y: 0), andScrollView: self.scrollview, andAnimationMargin: 0)
    }
    
    //MARK:- Past
    
    @IBAction func btnPayment(_ sender: UIButton) {
        
        sender.backgroundColor = AppColor.appColor
        sender.setTitleColor(.white, for: .normal)
        btn_Profile.backgroundColor = .clear
        btn_Profile.setTitleColor(AppColor.textColor, for: .normal)
        
        animateScrollViewHorizontally(destinationPoint: CGPoint(x: 1 * self.view.frame.width, y: 0), andScrollView: self.scrollview, andAnimationMargin: 0)
       
    }
    
    
    //MARK:- Custom Method
    
    func animateScrollViewHorizontally(destinationPoint destination: CGPoint, andScrollView scrollView: UIScrollView, andAnimationMargin margin: CGFloat) {
        
        var change: Int = 0;
        let diffx: CGFloat = destination.x - scrollView.contentOffset.x;
        var _: CGFloat = destination.y - scrollView.contentOffset.y;
        
        if(diffx < 0) {
            change = 1
        }
        else {
            change = 2
        }
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3);
        UIView.setAnimationCurve(.easeIn);
        switch (change) {
        case 1:
            scrollView.contentOffset = CGPoint(x: destination.x - margin, y: destination.y);
        case 2:
            scrollView.contentOffset = CGPoint(x: destination.x + margin, y: destination.y);
        default:
            return;
        }
        UIView.commitAnimations();
        let firstDelay: Double  = 0.3;
        let startTime: DispatchTime = DispatchTime.now() + Double(Int64(firstDelay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: startTime, execute: {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.2);
            UIView.setAnimationCurve(.linear);
            switch (change) {
            case 1:
                scrollView.contentOffset = CGPoint(x: destination.x + margin, y: destination.y);
            case 2:
                scrollView.contentOffset = CGPoint(x: destination.x - margin, y: destination.y);
            default:
                return;
            }
            UIView.commitAnimations();
            let secondDelay: Double  = 0.2;
            let startTimeSecond: DispatchTime = DispatchTime.now() + Double(Int64(secondDelay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: startTimeSecond, execute: {
                
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1);
                UIView.setAnimationCurve(.easeInOut);
                scrollView.contentOffset = CGPoint(x: destination.x, y: destination.y);
                UIView.commitAnimations();
                
            })
        })
    }
    
    func intialSetUp()
    {
        self.scrollview.contentSize = CGSize(width: 2*self.view.frame.width,height: self.scrollview.frame.height);
        
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        
        
        self.addChild(vc1)
        self.addChild(vc2)
        
        self.loadScrollView()
    }
    
    func loadScrollView ()
    {
        print(self.children)
        for index in 0 ..< self.children.count
        {
            self.loadScrollViewWithPage(index);
        }
    }
    
    func loadScrollViewWithPage(_ page : Int) -> Void
    {
        if(page < 0)
        {
            return
        }
        if page >= self.children.count
        {
            return
        }
        let viewController: UIViewController? = (self.children as NSArray).object(at: page) as? UIViewController
        if(viewController == nil)
        {
            return
        }
        if(viewController?.view.superview == nil){
            
            var frame: CGRect  = self.scrollview.frame
            frame.origin.x = self.view.frame.width*CGFloat(page)
            frame.origin.y = 0;
            viewController?.view.frame = frame;
            self.scrollview.addSubview(viewController!.view);
        }
    }
    
}

//MARK:- Api Implementation

extension MyProfileVC:serachTapDelegate{
    
    func serachTap() {
        
        alertWithHandler2(message: Constant.logoutMessage) {
            self.pageSetup()
        }
    }
    
    // Initial page settings
    func pageSetup()  {
        
        showActivityIndicator(view: view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.viewModel.getLogoutService()
            self.closureSetUp()
            
        }
    }
    // Closure initialize
       func closureSetUp()  {
           
           viewModel.errorMessage = { [weak self] (message)  in
               DispatchQueue.main.async {
                   print(message)
                   self?.handleError(message)
                  // self?.hideActivityIndicator()
               }
           }
       }
    
    func alertWithHandler2(message : String,block_Yes:  @escaping ()->Void){
        
        let alertController = UIAlertController(title:CommonClass.sharedInstance.createString(Str: "E-RX"), message: message, preferredStyle: .alert)
        
        // Create OK button
        let OKAction = UIAlertAction(title: comClass.createString(Str: "Yes"), style: .default) { (action:UIAlertAction!) in
            
            // Code in this block will trigger when OK button tapped.
            print("Ok button tapped");
            
            block_Yes()
            
        }
        alertController.addAction(OKAction)
        
        // Create Cancel button
        let cancelAction = UIAlertAction(title: comClass.createString(Str: "No"), style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alertController.addAction(cancelAction)
        alertController.view.backgroundColor = AppColor.appColor
        // Present Dialog message

        self.present(alertController, animated: true, completion:nil)
    }
}
