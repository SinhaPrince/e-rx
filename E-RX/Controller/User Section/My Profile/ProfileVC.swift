//
//  ProfileVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift
import ADCountryPicker

class ProfileVC: BaseViewController,UITextViewDelegate {

    //MARK:- Outlet
    
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet weak var lbl_NameHeader: UILabel!
    @IBOutlet weak var lbl_EmailHeader: UILabel!
    @IBOutlet weak var lbl_MobileHeader: UILabel!
    @IBOutlet weak var lbl_AddressHeader: UILabel!
    @IBOutlet weak var txtView_Address: UITextView!
    @IBOutlet weak var txt_Name: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_MobileNum: UITextField!
    @IBOutlet weak var btn_EditProfile: UIButton!
    @IBOutlet weak var txt_CountryCode: UITextField!
    @IBOutlet weak var btn_CountryCode: UIButton!
    @IBOutlet weak var btn_Address: UIButton!
    @IBOutlet weak var btn_Cam: UIButton!
    
    //MARK:- Variables
    var imagePicker = UIImagePickerController()
    var imgData = NSData()
    var lat : String?
    var long : String?
    let alert = UIAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setName()
        txtView_Address.delegate = self
        setdataonScreen()
        imagePicker.isEditing = true
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
    }
   
    //MARK:- Set UIName on UIElements
    //MARK:-
    
    private func setName(){
        
        
        LblFontSizeName(name: [.name,.email,.phoneNo,.address], lbl: [lbl_NameHeader,lbl_EmailHeader,lbl_MobileHeader,lbl_AddressHeader])
        
        txtFldFontSizeConfig(txtFld: [txt_Name,txt_Email,txt_MobileNum,txt_CountryCode], placeHolderText: [.EYN,.EYEID,.EYMNO,.countryCode])
        
        setDataOnButton(btn: [btn_EditProfile], text: [.EditProfile], textcolor: [AppColor.textColor])
        

    }

    //MARK:- TxtView Custom Method..
    fileprivate func txtViewDelegate(){
        
        txtView_Address.textColor = .lightGray
        txtView_Address.text = comClass.createString(Str: "Enter your address")
        txtView_Address.delegate = self
    }
    
    //MARK:- TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (txtView_Address.text == comClass.createString(Str: "Enter your address"))
            
        {
            txtView_Address.text = nil
            txtView_Address.textColor = AppColor.textColor
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtView_Address.text.isEmpty
        {
            txtView_Address.text = comClass.createString(Str: "Enter your address")
            txtView_Address.textColor = .lightGray
            
        }
        txtView_Address.resignFirstResponder()
    }
    
    private func setdataonScreen(){
        
        self.txt_Name.text = self.getInfoById().fullName
        self.txt_Email.text = self.getInfoById().email
        self.txtView_Address.text = self.getInfoById().address
        self.txt_MobileNum.text = self.getInfoById().mobileNumber
        self.txt_CountryCode.text = self.getInfoById().countryCode
        self.lat = self.getInfoById().latitude
        self.long = self.getInfoById().longitude
        self.img_Profile.setImage(withImageId: self.getInfoById().profilePic!, placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        
        DispatchQueue.global(qos: .background).async {
            if let theProfileImageUrl = self.getInfoById().profilePic {
                do {
                    guard let url = URL(string: theProfileImageUrl ) else {
                        return
                    }
                    self.imgData = try Data(contentsOf: url) as NSData
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
        }
       
        addDoneButtonOnKeyboard(txtFld: [txt_Email,txt_Name,txt_Name])
        
        //self.txtView_Address.translatesAutoresizingMaskIntoConstraints = true
        self.txtView_Address.sizeToFit()
        self.txtView_Address.isScrollEnabled = false
        
    }
    
    
    //MARK:- UIButton Action
    
    @IBAction func btn_TapAction(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeCameraPopUpVC") as! HomeCameraPopUpVC
            vc.delegateCamAndGall = self
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            self.navigationController?.present(vc, animated: false, completion: nil)
            
        }else if sender.tag == 2{
           
            let picker = ADCountryPicker()
            let pickerNavigationController = UINavigationController(rootViewController: picker)
            self.present(pickerNavigationController, animated: true, completion: nil)
            picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
                print(dialCode)
                self.txt_CountryCode.text = dialCode
                self.dismiss(animated: false, completion: nil)
            }
            
        }else if sender.tag == 3{
            
            if sender.isSelected == false{
                
                self.changeUI(bool: true)
                
            }else{
                
                pageSetup()
            }
           
            sender.isSelected = !sender.isSelected
            
            
        }else if sender.tag == 4{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddVC") as! SearchAddVC
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            vc.obj = self
            self.present(vc, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK:- Custom Method To Change Some UI Functionality
    
    private func changeUI(bool:Bool){
        
        self.btn_EditProfile.setTitle((bool == true ? comClass.createString(Str: "Save") : comClass.createString(Str: "Edit Profile")), for: .normal)
        self.btn_EditProfile.setTitleColor((bool == true ? .white : AppColor.textColor), for: .normal)
        self.btn_EditProfile.backgroundColor = (bool == true ? AppColor.appColor : #colorLiteral(red: 0.8166120648, green: 0.8239687085, blue: 0.8339691758, alpha: 1))
        self.txtFldMethod(bool: bool, txtFld: txt_Name)
        self.txtFldMethod(bool: bool, txtFld: txt_CountryCode)
        self.txtFldMethod(bool: bool, txtFld: txt_MobileNum)
        self.txtFldMethod(bool: bool, txtFld: txt_Email)
        self.btn_Cam.isUserInteractionEnabled = bool
        self.btn_Address.isUserInteractionEnabled = bool
        self.btn_CountryCode.isUserInteractionEnabled = bool
        
        self.txtView_Address.borderWidth = (bool == true ? 1 : 0)
        self.txtView_Address.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.txtView_Address.isUserInteractionEnabled = bool
        self.txtView_Address.contentInset = UIEdgeInsets(top: (bool == true ? 10 : 0), left: (bool == true ? 10 : 0), bottom: (bool == true ? 10 : 0), right: (bool == true ? 10 : 0))
       
    }
    
    func txtFldMethod(bool:Bool,txtFld:UITextField){
        
        txtFld.isUserInteractionEnabled = bool
        txtFld.layer.cornerRadius = 25
        txtFld.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        txtFld.borderWidth = (bool == true ? 1 : 0)
        txtFld.textAlignment = (bool == true ? .left : .right)
        let paddingPhoneNumber = UIView(frame: CGRect(x: 0, y: 0, width: (bool == true ? 25 : 0), height: txtFld.frame.height))
        txtFld.leftView = paddingPhoneNumber
        txtFld.leftViewMode = UITextField.ViewMode.always
    }
}

extension ProfileVC:UINavigationControllerDelegate,UIImagePickerControllerDelegate,cameraAndgalleryDelegate{
    
    
    func camAndGallery(type: String) {
        
        type == "Camera" ? comClass.openCamera(imagePicker: imagePicker, vc: self):comClass.openGallery(imagePicker: imagePicker, vc: self)
        
    }
    
    // MARK:- IMAGEPICKER DELEGATE
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
            if let imageData = chosenImage.jpegData(compressionQuality: 1.0) as NSData? {
                
               self.img_Profile.image = chosenImage
               self.imgData = imageData
                
            }else{
                print("imageData nahi aaya gya")
            }
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
        
   }
}

//MARK:- Api Implementation
extension ProfileVC{
    
    // Initial page settings
    func pageSetup()  {
        
        self.showActivityIndicator(view: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            let passDict = ["countryCode":self.txt_CountryCode.text ?? "",
                            "mobileNumber":self.txt_MobileNum.text ?? ""] as [String:Any]
            
            self.viewModel.getMobileNumberService(URl: APIEndpoint.checkMobileAvailability, passDict: passDict)
            self.closureSetUp()
            
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = { [weak self] (message) in
            
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.handleError(message)
                self?.changeUI(bool: false)
                
            }
        }
        
        viewModel.reloadList = { [weak self] () in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                let vc = self?.storyboard?.instantiateViewController(withIdentifier: "OtpVerificationVc") as! OtpVerificationVc
                vc.phoneNumber = ((self?.txt_CountryCode.text)! + ((self?.txt_MobileNum.text)!))
                vc.delegate = self
                vc.mobile = self?.txt_MobileNum.text
                self?.present(vc, animated: true, completion: nil)
                
            }
            
        }
        
        viewModel.updateProfile = {[weak self] () in
            
            self?.showActivityIndicator(view: self!.view)
            let passDict = ["email":self?.txt_Email.text ?? "",
                            "fullName":self?.txt_Name.text ?? "",
                            "address":self?.txtView_Address.text ?? "",
                            "longitude":self?.lat ?? "",
                            "latitude":self?.long ?? ""] as [String:Any]
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self?.viewModel.getUpdateProfileService(passDict: passDict, imageData: self!.imgData)
                self?.closureSetUp()
            }
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                
                if message == "Mobile number is registered"{ // Mobile already registered then we just only update their profile except mobile number
                    
                    self?.showActivityIndicator(view: self!.view)
                    let passDict = ["email":self?.txt_Email.text ?? "",
                                    "fullName":self?.txt_Name.text ?? "",
                                    "address":self?.txtView_Address.text ?? "",
                                    "longitude":self?.lat ?? "",
                                    "latitude":self?.long ?? ""] as [String:Any]
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self?.viewModel.getUpdateProfileService(passDict: passDict, imageData: self!.imgData)
                        self?.closureSetUp()
                    }
                    
                }else{
                    self?.handleError(message)
                }
            }
        }
    }
    
    
    
    
}

//MARK:- Custom Protocol Methods
//MARK:-

extension ProfileVC:passAddress,OTPAuthenticationDelegate{
    
    func otpAuthCompletion() {
        
        self.showActivityIndicator(view: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            let passDict = ["countryCode":self.txt_CountryCode.text ?? "",
                            "mobileNumber":self.txt_MobileNum.text ?? ""] as [String:Any]
            
            self.viewModel.getChangeMobileNumberService(URl: APIEndpoint.changeMobileNumber, passDict: passDict)
            self.closureSetUp()
            
        }
        
    }
    
    func backAddressToVC(Address: String, addressType: String, lat: String, long: String) {
        
        self.txtView_Address.text = Address
        self.lat = lat
        self.long = long
    }
    
    
}


