//
//  PrescriptionInvoiceVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import Cosmos
import RealmSwift

class PrescriptionInvoiceVC: BaseViewController {

    
    //MARK:- Outlet

    @IBOutlet weak var lbl_ServiceName : UILabel!
    @IBOutlet weak var lbl_Date : UILabel!
    @IBOutlet weak var lbl_Time : UILabel!
    @IBOutlet weak var lbl_PayMethod : UILabel!
    @IBOutlet weak var lbl_SerProDetail : UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var lbl_MobileNum: UILabel!
    @IBOutlet weak var btn_Invoice : UIButton!
    @IBOutlet weak var lbl_ServiceProDetailsHeader: UILabel!
    
    //MARK:- Variables
    var prescriptionId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Internet Checking Functionality
        defaultConfiguration()
        self.delegateWarningMessageView = self
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initialMethod()
        showHideView(view: [lbl_Date,lbl_Time,lbl_ServiceName,lbl_SerProDetail,lbl_PayMethod,rateView,btn_Invoice,lbl_MobileNum], hide: true)
        setNavi()
    }
    
    //MARK:- Setup navigationbar
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("Prescription ID", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
    
    //MARK:- Set UIName on UIElements
    //MARK:-
    
    private func setName(){
        
        let data = viewModel.prescriptionDetailsArray[0].data
        let ref = self.showWarningMessageView()
        ref.removeFromSuperview()
        
        showHideView(view: [lbl_Date,lbl_Time,lbl_ServiceName,lbl_SerProDetail,lbl_PayMethod,rateView,btn_Invoice,lbl_MobileNum], hide: false)
        
        
        let datetime = comClass.dateTimeConversion(createdAt: data?.updatedAt ?? "")
        setApiData(text: ["\(comClass.createString(Str: "Service Name")):  Speedex","\(comClass.createString(Str: "Date")):  \(datetime.prefix(10))","\(comClass.createString(Str: "Time")):  \(datetime.suffix(8))","\(comClass.createString(Str: "Payment method")):  \(data?.paymentType ?? "")",("\(comClass.createString(Str: "Name")):  \(data?.providerId?.fullName ?? "")"),("\(comClass.createString(Str: "Mobile Number")): \((data?.providerId?.countryCode ?? "") + (data?.providerId?.mobileNumber ?? ""))"),comClass.createString(Str: "Service Provider Details")], lbl: [lbl_ServiceName,lbl_Date,lbl_Time,lbl_PayMethod,lbl_SerProDetail,lbl_MobileNum,lbl_ServiceProDetailsHeader])
        self.rateView.rating = Double(data?.providerId?.avgRating ?? 0)
        setDataOnButton(btn: [btn_Invoice], text: [.Invoice], textcolor: [AppColor.appColor])
        
        
    }

    func setApiData(text:[String],lbl:[UILabel]){
        
        for i in 0..<text.count{
            comClass.LblFontSizeName(name: text[i], lbl: lbl[i], textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 15)
        }
        
    }

}
//MARK:- Functions
extension PrescriptionInvoiceVC:TryAgainDeleagte,LoadWarningMessageView{
    
    func tryAgain() {
       initialMethod()
    }
    
    func loadWarinigView() {
        
        let ref = self.showWarningMessageView(self.view, "Sorry,ERx requires an Internet Connection!")
        ref.delegate = self
    }
    
    
    func initialMethod() {
        
         showHideView(view: [lbl_Date,lbl_Time,lbl_ServiceName,lbl_SerProDetail,lbl_PayMethod,rateView,btn_Invoice,lbl_MobileNum], hide: true)
        
        // Call pageSetup
        pageSetup()
        self.showActivityIndicator(view: self.view)
    }
    
    // Initial page settings
    func pageSetup()  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            let passDict = ["prescriptionId":self.prescriptionId ?? ""] as [String:Any]
            let header = ["Authorization":self.getInfoById().Token] as! [String:String]
            
            self.viewModel.getPrescriptionDetailServiceCall(URL: "prescriptionDetail", Parameters: passDict, Header: header)
            self.closureSetUp()
            
        }
    }
    // Closure initialize
    func closureSetUp()  {
        viewModel.reloadList = { [weak self] ()  in
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.setName()
                
            }
        }
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
            }
        }
    }
}
