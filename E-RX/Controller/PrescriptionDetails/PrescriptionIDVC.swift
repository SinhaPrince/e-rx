//
//  PrescriptionIDVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class PrescriptionIDVC: BaseViewController {
    
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Number: UILabel!
    @IBOutlet weak var lbl_PrescriptionDesp: UILabel!
    @IBOutlet weak var btn_OFD: UIButton!
    @IBOutlet weak var btn_ChatDBoy: UIButton!
    @IBOutlet weak var lbl_ChatProvider: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var rate: UIButton!
    @IBOutlet weak var img_Prescription: UIImageView!
    
    //MARK:- Variables
    var prescriptionId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Internet Checking Functionality
        defaultConfiguration()
        self.delegateWarningMessageView = self
        initialMethod()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavi()
        
    }
    
    //MARK:- Setup navigationbar
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("Prescription ID", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
    
    //MARK:- Set UIName on UIElements
    //MARK:-
    
    func setName(){
        
        let ref = self.showWarningMessageView()
        ref.removeFromSuperview()
        
        showHideView(view: [lbl_Name,lbl_Number,btn_OFD,btn_ChatDBoy,lbl_ChatProvider,lbl_PrescriptionDesp,img_Prescription,imgView,rate], hide: false)
        
        let dataItems = viewModel.prescriptionDetailsArray[0].data
        
        fontAndColor(lbl: [lbl_Name,lbl_Number,lbl_PrescriptionDesp], text: [(dataItems?.providerId?.fullName)!,(dataItems?.providerId?.mobileNumber)!,(dataItems?.address ?? "")])
        
        lbl_PrescriptionDesp.text = "Booking Date: \(comClass.dateTimeConversion(createdAt: dataItems?.createdAt ?? ""))\nDelivery Type: \(dataItems?.deliveryType ?? "")\nAddress: \(dataItems?.address ?? "")"
        
        lbl_Name.font = AppFont.Regular.size(AppFontName.Lato, size: 17)
        lbl_Name.textColor = AppColor.textColor
        
        setDataOnButton(btn: [btn_OFD,lbl_ChatProvider,btn_ChatDBoy], text: [.OFD,.CWP,.CWDB], textcolor: [AppColor.appColor,AppColor.whiteColor,AppColor.whiteColor])
        
        btn_ChatDBoy.contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        lbl_ChatProvider.contentEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        
        self.imgView.setImage(withImageId: (dataItems?.providerId?.profilePic)!, placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        self.img_Prescription.setImage(withImageId: dataItems?.prescriptionImage ?? "", placeholderImage: #imageLiteral(resourceName: "pres_s"))
        self.rate.setTitle(String((dataItems?.providerId?.avgRating)!), for: .normal)
        self.btn_ChatDBoy.isHidden = dataItems?.deliveryType == "Pick Up" ? true : false
        btn_OFD.setTitle(dataItems?.prescriptionStatus, for: .normal)
    }
    
    
    
    @IBAction func btn_ChatWP(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.chatType = "Chat with Provider"
        vc.items = viewModel.prescriptionDetailsArray[0].data
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func btn_ChatWDB(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.chatType = "Chat with Delivery Boy"
        vc.items = viewModel.prescriptionDetailsArray[0].data
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func tapImgPrescription(_ sender: UIButton) {
        
        let imageInfo   = GSImageInfo(image: self.img_Prescription.image ?? #imageLiteral(resourceName: "pres_s"), imageMode: .aspectFit)
        let transitionInfo = GSTransitionInfo(fromView: sender)
        let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
        
        imageViewer.dismissCompletion = {
            print("dismissCompletion")
        }
        present(imageViewer, animated: true, completion: nil)
    }
    
}

//MARK:- Functions
extension PrescriptionIDVC:TryAgainDeleagte,LoadWarningMessageView{
    
    func tryAgain() {
        initialMethod()
    }
    
    func loadWarinigView() {
        let ref = self.showWarningMessageView(self.view, "Sorry,ERx requires an Internet Connection!")
        ref.delegate = self
    }
    
    func initialMethod() {
        
        showHideView(view: [lbl_Name,lbl_Number,btn_OFD,btn_ChatDBoy,lbl_ChatProvider,lbl_PrescriptionDesp,img_Prescription,imgView,rate], hide: true)
        
        // Call pageSetup
        pageSetup()
        self.showActivityIndicator(view: self.view)
    }
    
    // Initial page settings
    func pageSetup()  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            
            let passDict = ["prescriptionId":self.prescriptionId ?? ""] as [String:Any]
            let header = ["Authorization":self.getInfoById().Token] as! [String:String]
            
            self.viewModel.getPrescriptionDetailServiceCall(URL: "prescriptionDetail", Parameters: passDict, Header: header)
            self.closureSetUp()
            
        }
    }
    // Closure initialize
    func closureSetUp()  {
        viewModel.reloadList = { [weak self] ()  in
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.setName()
                
                //self?.hideActivityIndicator()
            }
        }
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
    }
}
