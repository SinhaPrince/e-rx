//
//  SettingVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class SettingVC: BaseViewController {

    
    //MARK:- Outlet
    
    @IBOutlet weak var privacyPolicy: UIButton!
    @IBOutlet weak var termsAndConditn: UIButton!
    @IBOutlet weak var switchbar: UISwitch!
    @IBOutlet weak var lblSupport: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var btn_Push: UIButton!
    @IBOutlet weak var btn_TC: UIButton!
    @IBOutlet weak var btn_PP: UIButton!
    
    //MARK:- Variables
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setConfiguration()
        setupNavi()
        
    }
    
    
    //MARK:- navigation setup
    
    func setupNavi(){
        hideNavigationBar(false)
        setupNavigationBarTitle("Settings", leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    //MARK:- Configure uielements
    
    private func setConfiguration(){
        
        LblFontSizeName(name: [.aboutUs,.contactUs,.support], lbl: [lblAboutUs,lblContact,lblSupport])
        setDataOnButton(btn: [btn_Push,btn_TC,btn_PP], text: [.pushNoti,.TOS,.privacyPolicy], textcolor: [AppColor.textColor,#colorLiteral(red: 0.08232767135, green: 0.381834805, blue: 0.8324789405, alpha: 1),#colorLiteral(red: 0.08232767135, green: 0.381834805, blue: 0.8324789405, alpha: 1)])
       
        self.switchbar.isOn = self.getInfoById().notificationStatus == "true" ? true:false
        self.switchbar.addTarget(self, action: #selector(switchTap(_:)), for: .touchUpInside)
        
    }
    
    @objc func switchTap(_ sender:UISwitch){
        
        pageSetup(notificationStatus: String(sender.isOn))
        
    }

    
    @IBAction func btnChangeLang(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageSelectionVC") as! LanguageSelectionVC
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnClick(_ sender: UIButton){
        
        let vc = WebViewVC()
        if sender.tag == 1 {
            vc.item = webViewDataModel(strURL: "http://3.22.227.41/about.html", title: "About Us")
        }else if sender.tag == 2{
            vc.item = webViewDataModel(strURL: "http://e-rx.cc/contact.html", title: "Contact Us")
        }else if sender.tag == 3{
            vc.item = webViewDataModel(strURL: "http://3.22.227.41/support.html", title: "Support")
        }else if sender.tag == 4{
            vc.item = webViewDataModel(strURL: "http://e-rx.cc/terms.html", title: "Terms and Conditions")
        }else if sender.tag == 5{
            vc.item = webViewDataModel(strURL: "http://3.22.227.41/privacy.html", title: "Privacy Policy")
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
//MARK:- Api Notification Service
//MARK:-
extension SettingVC{
    
      // Initial page settings
    func pageSetup(notificationStatus:String)  {
        
        print(notificationStatus)
        
          showActivityIndicator(view: view)
          DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
              // API calling from viewmodel class
            self.viewModel.getStatusUpdateService(APIEndpoint.changeNotificationStatus,passDict: ["notificationStatus" :notificationStatus])
              self.closureSetUp()
              
          }
      }
      
      // Closure initialize
      func closureSetUp()  {
          
          viewModel.success = {[weak self] (message) in
            
            DispatchQueue.main.async {
                try! self?.realm.write {
                    print(String(self?.switchbar.isOn ?? false))
                    self?.getInfoById().notificationStatus = String(self?.switchbar.isOn ?? false)
                    self?.handleError(message)
                }
            }
              
          }
          
          viewModel.errorMessage = { [weak self] (message)  in
              DispatchQueue.main.async {
                  print(message)
                  self?.handleError(message)
                  // self?.hideActivityIndicator()
              }
          }
      }
}
