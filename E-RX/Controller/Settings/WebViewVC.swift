//
//  WebViewVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 17/08/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import WebKit

struct webViewDataModel {
    var strURL: String
    var title: String
}

class WebViewVC: BaseViewController {
    
    var item: webViewDataModel? {
        didSet{
            self.setupNavi(item?.title ?? "")
            self.webViewSetup(item?.strURL ?? "")
        }
    }
    
    //MARK:- navigation setup
    
    func setupNavi(_ title: String){
        hideNavigationBar(false)
        setupNavigationBarTitle("\(title)", leftBarButtonsType: [.back], rightBarButtonsType: [])
    }
    
    internal func webViewSetup(_ strURL:String){
        guard let url = URL(string: strURL) else {
            return
        }
        let request = URLRequest(url: url)
        let webView = WKWebView(frame: CGRect(x: 0, y: 20, width: view.bounds.width, height: view.bounds.height - 20))
        webView.navigationDelegate = self
        webView.load(request)
        webView.contentMode = .bottomRight
        webView.configuration.userContentController.addUserScript(self.getZoomDisableScript())
        self.view.addSubview(webView)
    }
    
    private func getZoomDisableScript() -> WKUserScript {
        let source: String = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum- scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" + "head.appendChild(meta);"
        return WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
    }
}

//MARK:- WKWebView Delegate Setup
//MARK:-
extension WebViewVC:WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showActivityIndicator(view: view)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideActivityIndicator()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        hideActivityIndicator()
    }
}
