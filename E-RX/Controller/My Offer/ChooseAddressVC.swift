//
//  ChooseAddressVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 16/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class ChooseAddressVC: BaseViewController,UITextViewDelegate {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_ExAddressHader: UILabel!
    @IBOutlet weak var lbl_ExAddress: UILabel!
    @IBOutlet weak var lbl_DeliveryAtAddHeader: UILabel!
    @IBOutlet weak var lbl_NewAdress: UILabel!
    @IBOutlet weak var lbl_Or: UILabel!
    @IBOutlet weak var txt_ViewAddress: UITextView!
    @IBOutlet weak var btn_Proceed: UIButton!
    @IBOutlet weak var btn_ExAddress: UIButton!
    @IBOutlet weak var btn_NewAddress: UIButton!
    
    //MARK:- Variables
    
    var lat: String?
    var long: String?
   
    var backAddressDelegate: passAddress?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        defaultConfiguration()
        setNavi()
        setData()
    }
    
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("Choose Address", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
    
    internal func setData(){
        
        comClass.LblFontSizeName(name: "Existing Address", lbl: lbl_ExAddressHader, textColor: .black, fontName: comClass.BoldFont, fontSize: 17)
        comClass.LblFontSizeName(name: "Delivery at New Address", lbl: lbl_DeliveryAtAddHeader, textColor: .black, fontName: comClass.BoldFont, fontSize: 17)
        comClass.LblFontSizeName(name: "OR", lbl: lbl_Or, textColor: AppColor.placeHolderColor, fontName: comClass.RegularFont, fontSize: 16)
        setDataOnButton(btn: [btn_Proceed], text: [.proceed], textcolor: [.white])
        txtViewDelegate(txtView: txt_ViewAddress)
        comClass.LblFontSizeName(name: self.getInfoById().address ?? "", lbl: lbl_ExAddress, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 15)
       
        self.btn_Proceed.addTarget(self, action: #selector(btnProceedTap), for: .touchUpInside)
        self.btn_NewAddress.addTarget(self, action: #selector(btnNewAddTap), for: .touchUpInside)
        self.btn_ExAddress.addTarget(self, action: #selector(btnExistingAdd(_:)), for: .touchUpInside)
    }
    
    //MARK:- Button Action
    
    @objc func btnExistingAdd(_ sender:UIButton){
        
        self.lat = self.getInfoById().latitude ?? ""
        self.long = self.getInfoById().longitude ?? ""
        sender.isSelected ? sender.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal) : sender.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        sender.isSelected = !sender.isSelected
    }
    @objc func btnNewAddTap(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddVC") as! SearchAddVC
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        vc.obj = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func btnProceedTap(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NoRefundPolicyPopup") as! NoRefundPolicyPopup
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
        
    }
    //MARK:- TxtView Custom Method..
    func txtViewDelegate(txtView:UITextView){
        txtView.textColor = .lightGray
        txtView.text = comClass.createString(Str: "Enter your address")
        txtView.font = AppFont.Regular.size(AppFontName.Lato, size: 16)
        txtView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: -10, right: -10)
        txtView.delegate = self
    }
    
    //MARK:- TextView Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (textView.text == comClass.createString(Str: "Enter your address"))
        {
            textView.text = nil
            textView.textColor = AppColor.textColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            textView.text = comClass.createString(Str: "Enter your address")
            textView.textColor = .lightGray
        }
        textView.resignFirstResponder()
    }
}

extension ChooseAddressVC:passAddress,termsConditionsDelegate{
    
    func termsCondition() {
        self.backAddressDelegate?.backAddressToVC(Address: (lbl_NewAdress.text == "" ? lbl_ExAddress.text : lbl_NewAdress.text) ?? "", addressType: "Delivery", lat: self.lat ?? "", long: self.long ?? "")
        self.navigationController?.popViewController(animated: false)
    }
    
    func backAddressToVC(Address: String, addressType: String, lat: String, long: String) {
        
        comClass.LblFontSizeName(name: Address, lbl: lbl_NewAdress, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 15)
        self.lat = lat
        self.long = long
        btn_ExAddress.isSelected = !btn_ExAddress.isSelected
        btn_ExAddress.isSelected ? btn_ExAddress.setImage(#imageLiteral(resourceName: "tick"), for: .normal) : btn_ExAddress.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal)
        
    }
    
    
}
