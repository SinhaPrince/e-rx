

import UIKit
import RealmSwift

class MyOfferVC: BaseViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Varibale
    var index: Int?
    var deliveryType: String?
    var lat: String?
    var long: String?
    var address: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Internet Checking Functionality
        defaultConfiguration()
        self.delegateWarningMessageView = self
        setNavi()
        initialMethod()
    }
    
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("My Offers", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
    
}

//MARK:- UITableView Datasource and Delegate
//MARK:-

extension MyOfferVC:UITableViewDelegate,UITableViewDataSource{
    
    
    //MARK:- numberOfRowsInSection
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.myOfferListArr[0].data?.count ?? 0
    }
    
    //MARK:- cellForRowAt
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOfferCell", for: indexPath) as! MyOfferCell
        cell.setDataOnScreen((viewModel.myOfferListArr[0].data?[indexPath.row])!)
        cell.btn_Accept.addTarget(self, action: #selector(btnAcceptTap(_:)), for: .touchUpInside)
        cell.btn_Reject.addTarget(self, action: #selector(btnRejectTap(_:)), for: .touchUpInside)
        cell.btn_Reject.tag = indexPath.row
        cell.btn_Accept.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
        
    }
    
    @objc func btnAcceptTap(_ sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AcceptPopupVC") as! AcceptPopupVC
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        vc.items = viewModel.myOfferListArr[0].data?[sender.tag]
        vc.index = sender.tag
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
    @objc func btnRejectTap(_ sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RejectPopUpVC") as! RejectPopUpVC
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        vc.items = viewModel.myOfferListArr[0].data?[sender.tag]
        vc.index = sender.tag
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
        
    }
}

//MARK:- Api Calling from ViewModel
//MARK:-

extension MyOfferVC{
    
    func initialMethod() {
        
        tableView.register(UINib(nibName: "MyOfferCell", bundle: nil), forCellReuseIdentifier: "MyOfferCell")
        // Call pageSetup
        pageSetup()
    }
    
    // TableViewSetUp
    func tableViewSetup()  {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = false
        self.tableView.reloadData()
    }
    
    // Initial page settings
    func pageSetup()  {
        
        showActivityIndicator(view: view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            self.viewModel.getprescriptionOfferListService()
            self.closureSetUp()
            
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        viewModel.reloadList = { [weak self] ()  in
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.tableViewSetup()
                
                if self?.viewModel.myOfferListArr[0].data?.count == 0 {
                    let ref = self?.showWarningMessageView(self!.view, "No Data Found!")
                    ref?.delegate = self
                }else{
                    let ref = self?.showWarningMessageView()
                    ref?.removeFromSuperview()
                }
                
            }
        }
        
        viewModel.success = {[weak self] (message) in
            
            if message == "Charge Run Successfully"{
                //Tap Payment Started
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TapPaymentVC") as! TapPaymentVC
                    vc.chargeModelArr = self?.viewModel.chargeDataArr[0]
                    vc.prescriptionOfferId = self?.viewModel.myOfferListArr[0].data?[self?.index ?? 0].Id
                    vc.deliveryType = self?.deliveryType
                    vc.delegate = self
                    if #available(iOS 13.0, *) {
                        vc.modalPresentationStyle = .formSheet
                    }
                    self?.navigationController?.present(vc, animated: true, completion: nil)
                }
                
            }else if message == "Payment Successfully"{
                
                self?.handleError(message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if self?.deliveryType == "Delivery"{
                        self?.pageSetup(deliveryType: "Delivery", lat: self?.lat ?? "", long: self?.long ?? "", address: self?.address ?? "")
                    }else{
                        self?.pageSetup(deliveryType: "Pick Up", lat: self?.getInfoById().latitude ?? "", long: self?.getInfoById().longitude ?? "", address: self?.getInfoById().address ?? "")
                    }
                }
                
            }
            else{
                self?.handleError(message)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self?.pageSetup()
                }
            }
        }

        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
            }
        }
    }
}

//MARK:- Order Protocol Calling

extension MyOfferVC: OrderTypeDelegate,passAddress,paymentDoneDelegate{
    
    //payDone Call after payment process completed
    // Accept Oder Api Service Calling
    func payDone(id: String) {
        
         pageSetup(paymentId: "Pay987654321", paymentType: "Online", status: "Success", prescriptionOfferId: id)
        
    }
    
    //For Delivery Type Take address ChooseAddressVC
    func backAddressToVC(Address: String, addressType: String, lat: String, long: String) {
        self.lat = lat
        self.long = long
        self.address = Address
        //Charge Api Calling
        pageSetup(prescriptionOfferId: self.viewModel.myOfferListArr[0].data?[self.index ?? 0].Id ?? "",deliveryType:"Delivery")
        
    }
    
    //Call after choose delivery type From AcceptPopupVC
    func orderType(type: String,index:Int) {
        
        self.index = index
        
        if type == "Delivery"{
            
            self.deliveryType = "Delivery"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseAddressVC") as! ChooseAddressVC
            vc.backAddressDelegate = self
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else if type == "Pickup"{
           self.deliveryType = "Pick Up"
            
            //MARK:- Charge Api Service Calling
            pageSetup(prescriptionOfferId: self.viewModel.myOfferListArr[0].data?[index].Id ?? "",deliveryType:"Pick Up")
           
           
        }else{
            // if order rejected..
             handleError("Prescription request cancelled successfully")
             pageSetup()
        }
        
    }
    
}

//MARK:- Api Calling
//MARK:-
extension MyOfferVC:TryAgainDeleagte,LoadWarningMessageView{
    
    func tryAgain() {
        pageSetup()
    }
    
    func loadWarinigView() {
        self.tableView.isHidden = true
        let ref = self.showWarningMessageView(self.view, "Sorry,ERx requires an Internet Connection!")
        ref.delegate = self
    }
    
    //MARK:- acceptPrescriptionRequest Service
    
    internal func pageSetup(deliveryType:String,lat:String,long:String,address:String){
        
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            
            let data = self.viewModel.myOfferListArr[0].data?[self.index ?? 0]
            let dict = ["prescriptionOfferId":data?.Id,"latitude":lat,"longitude":long,"address":address,"deliveryType":deliveryType,"amount":String(data?.amount ?? 0),"date":"2020-06-03","currency":data?.currency
                ,"paymentId":"Payment989898","paymentType":"Online","status":"SUCCESS"
            ]
            self.viewModel.getPrescriptionRequest(APIEndpoint.acceptPrescriptionRequest,dict as [String : Any])
            self.closureSetUp()
        }
        
    }
    
}

//MARK:- PaymentDone Protocol
protocol paymentDoneDelegate {
    func payDone(id: String)
}
