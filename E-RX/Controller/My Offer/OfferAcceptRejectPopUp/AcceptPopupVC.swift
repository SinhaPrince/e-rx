//
//  AcceptPopupVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 15/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class AcceptPopupVC: BaseViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var btn_PickUp: UIButton!
    @IBOutlet weak var btn_Delivery: UIButton!
    @IBOutlet weak var lbl_BottomDescription: UILabel!
    
    //MARK:- Variables
    var items : DataModel?
    var delegate:OrderTypeDelegate?
    var index:Int?
    
    override func viewDidLoad() {
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setData()
    }
    
    
    //MARK:- SetData On Screen
    internal func setData(){
        
        comClass.LblFontSizeName(name: "Choose Delivery Type", lbl: lbl_Header, textColor: AppColor.textColor, fontName: comClass.BoldFont, fontSize: 17)
        setDataOnButton(btn: [btn_PickUp,btn_Delivery], text: [.Pickup,.WantDelivery], textcolor: [.white,.white])
        self.lbl_BottomDescription.isHidden = true
        self.btn_PickUp.addTarget(self, action: #selector(btnPickupTap), for: .touchUpInside)
        self.btn_Delivery.addTarget(self, action: #selector(btnDeliveryTap), for: .touchUpInside)
    }
    
    //MARK:- Button Action
    //MARK:- Pickup
    @objc func btnPickupTap(){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NoRefundPolicyPopup") as! NoRefundPolicyPopup
        vc.delegate = self
        if #available(iOS 13.0, *) {
        vc.modalPresentationStyle = .formSheet
        }
        self.present(vc, animated: false, completion: nil)
        
    }
    //MARK:- Delivery
    @objc func btnDeliveryTap(){
        
        dismiss(animated: true) {
            self.delegate?.orderType(type: "Delivery",index:self.index ?? 0)
        }
        
    }
   
   
}

//MARK:- Api termsConditionsDelegate
//MARK:-
extension AcceptPopupVC:termsConditionsDelegate{
    
    func termsCondition() {
        dismiss(animated: true) {
            self.delegate?.orderType(type: "Pickup",index:self.index ?? 0)
        }
    }
    
}
