//
//  RejectPopUpVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 15/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class RejectPopUpVC: BaseViewController,UITextViewDelegate {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var btn_Provider: UIButton!
    @IBOutlet weak var btn_Payment: UIButton!
    @IBOutlet weak var btn_ChangeMind: UIButton!
    @IBOutlet weak var btn_Others: UIButton!
    @IBOutlet weak var txt_View: UITextView!
    @IBOutlet weak var btn_Submit: UIButton!
    
    //MARK:- Variables
    var items : DataModel?
    var reason:String?
    var delegate:OrderTypeDelegate?
    var index:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSwipGesture(view: view)
        // Do any additional setup after loading the view.
        self.btn_ChangeMind.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    internal func setData(){
        
        comClass.LblFontSizeName(name: "Reason for Cancellation", lbl: lbl_Header, textColor: AppColor.textColor, fontName: comClass.BoldFont, fontSize: 17)
        txt_View.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: -10, right: -10)
        txt_View.font = AppFont.Regular.size(AppFontName.Lato, size: 15)
        txt_View.textColor = AppColor.textColor
        
        setDataOnButton(btn: [btn_Provider,btn_Payment,btn_ChangeMind,btn_Others,btn_Submit], text: [.providerChange,.paymentIssue,.changeMind,.otherReason,.submit], textcolor: [AppColor.textColor,AppColor.textColor,AppColor.textColor,AppColor.textColor,.white])
        btn_Provider.titleLabel?.font = AppFont.Regular.size(AppFontName.Lato, size: 13)
         btn_Payment.titleLabel?.font = AppFont.Regular.size(AppFontName.Lato, size: 13)
         btn_Others.titleLabel?.font = AppFont.Regular.size(AppFontName.Lato, size: 13)
        txtViewDelegate()
        self.btn_ChangeMind.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        self.btn_Provider.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        self.btn_Payment.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        self.btn_Others.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        self.btn_Submit.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        
        
    }
    
    //MARK:- TxtView Custom Method..
    fileprivate func txtViewDelegate(){
        
        txt_View.textColor = .lightGray
        txt_View.text = comClass.createString(Str: "Enter your reason")
        txt_View.delegate = self
    }
    
    //MARK:- TextView Delegate
    
    private func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (txt_View.text == "Enter your reason")
        {
            txt_View.text = nil
            txt_View.textColor = AppColor.textColor
        }
    }
    
    private func textViewDidEndEditing(_ textView: UITextView) {
        
        if txt_View.text.isEmpty
        {
            txt_View.text = "Enter your reason"
            txt_View.textColor = .lightGray
        }
        txt_View.resignFirstResponder()
    }
    
    @objc func btnClick(_ sender:UIButton){
        
        if sender.tag == 1 {
            self.selectReason(btn1: sender, btn2: btn_Payment, btn3: btn_ChangeMind, btn4: btn_Others)
            self.reason = "Not all the medications are available"
        }else if sender.tag == 2 {
            self.selectReason(btn1: sender, btn2: btn_Provider, btn3: btn_ChangeMind, btn4: btn_Others)
            self.reason = "Provider is so far"
        }else if sender.tag == 3 {
            self.selectReason(btn1: sender, btn2: btn_Provider, btn3: btn_Payment, btn4: btn_Others)
            self.reason = "Change Mind"
        }else if sender.tag == 4{
            self.selectReason(btn1: sender, btn2: btn_Provider, btn3: btn_Payment, btn4: btn_ChangeMind)
            self.reason = "Other"
        }else{
            
            if self.reason != nil {
                pageSetup()
            }else{
                self.handleError("Select any reason for cancellation")
            }
            
        }
    }
  
    func selectReason(btn1:UIButton,btn2:UIButton,btn3:UIButton,btn4:UIButton){
        btn1.setImage(#imageLiteral(resourceName: "radio_s"), for: .normal)
        btn2.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal)
        btn3.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal)
        btn4.setImage(#imageLiteral(resourceName: "radio_un"), for: .normal)
    }
}

//MARK:- Api Calling
//MARK:-
extension RejectPopUpVC{
    
    internal func pageSetup(){
        
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            let passDict = ["prescriptionOfferId" : self.items?.Id ?? "","cancelReason":self.reason ?? "","cancelMessage":self.txt_View.text ?? "" == "Enter your reason" ? "" : self.txt_View.text] as [String:Any]
            self.viewModel.getPrescriptionRequest(APIEndpoint.prescriptionRequestReject,passDict)
            self.clouserSetup()
        }
        
    }
    internal func clouserSetup(){
        
        viewModel.success = {[weak self] (message) in
            
            self?.dismiss(animated: true, completion: {
                self?.delegate?.orderType(type: "Cancel",index:self?.index ?? 0)
            })
           
        }
        viewModel.errorMessage = {[weak self] (message) in
            
            self?.handleError(message)
            
        }
    }
    
}
