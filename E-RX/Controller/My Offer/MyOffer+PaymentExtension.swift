//
//  MyOffer+PaymentExtension.swift
//  E-RX
//
//  Created by SinhaAirBook on 07/08/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import UIKit
import WebKit

extension MyOfferVC{
    
    //MARK:- For Charge Api Service
    public func pageSetup(prescriptionOfferId: String, deliveryType: String){
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            let dict = ["prescriptionOfferId": prescriptionOfferId ,"deliveryType": deliveryType ] as [String:Any]
            self.viewModel.getPaymentChargeService(passDict: dict)
            self.closureSetUp()
        }
    }
}

class TapPaymentVC: BaseViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var TapBtn: UIButton!
    @IBOutlet weak var bottomView: UIView!
    
    //MARK:- Varibale
    
    var chargeModelArr: ChargeDataModel?
    var prescriptionOfferId: String?
    var deliveryType: String?
    var delegate: paymentDoneDelegate?
    let webView = WKWebView()
    
    override func viewDidLoad() {
        
        tapPaymentInitilization()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    internal func tapPaymentInitilization(){
       
        comClass.LblFontSizeName(name: "Tap on Pay Button to proceed", lbl: self.lbl_Name, textColor: .white, fontName: comClass.BoldFont, fontSize: 22)
        comClass.setDataOnButton(btn: TapBtn, text: "PAY \(chargeModelArr?.data?.chargeData?.currency?.uppercased() ?? "") \(chargeModelArr?.data?.chargeData?.amount ?? 0.0)", font: comClass.BoldFont, size: 20, textcolor: UIColor.white, image: UIImage(), backGroundColor: UIColor(), aliment: .center)
        self.TapBtn.addTarget(self, action: #selector(tapButtonClick), for: .touchUpInside)

    }
    
    internal func loadWKWebView(){
        
        self.bottomView.isHidden = true // Pay Button View
        guard let url = URL(string: self.chargeModelArr?.data?.chargeData?.transaction?.url ?? "") else { return }
        let request = URLRequest(url: url)
        webView.frame = view.bounds
        webView.navigationDelegate = self
        webView.load(request)
        view.addSubview(webView)
        
    }
    
    @objc func tapButtonClick(){
        loadWKWebView()
    }
}

extension TapPaymentVC: WKNavigationDelegate {
   
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showActivityIndicator(view: view)
        print("Started to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideActivityIndicator()
        let url = webView.url?.absoluteString
        print("---Hitted URL--->\(url!)") // here you are getting URL
        
        if url?.contains("http://3.22.227.41/erx-provider-version/#/") ?? false {
            self.dismiss(animated: true, completion: {
                self.delegate?.payDone(id: self.prescriptionOfferId ?? "")
            })
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.hideActivityIndicator()
        print(error.localizedDescription)
        
    }
}

//MARK:- paymentApi Service Calling
extension MyOfferVC{
    
    func pageSetup(paymentId: String,paymentType: String,status: String,prescriptionOfferId: String){
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let dict = [
                "prescriptionOfferId":"\(prescriptionOfferId)",
                "providerAmount":"\(self.viewModel.chargeDataArr[0].data?.providerAmount ?? 0)",
                "companyAmount":"\(self.viewModel.chargeDataArr[0].data?.companyAmount ?? 0)",
                        "adminAmount":"\(self.viewModel.chargeDataArr[0].data?.adminAmount ?? 0.0)",
                        "amount":"\(self.viewModel.chargeDataArr[0].data?.finalAmount ?? 0.0)",
                        "date":"25-07-020",
                        "currency":"\(self.viewModel.chargeDataArr[0].data?.chargeData?.currency ?? "")",
                        "paymentId":"\(paymentId)",
                        "paymentType":"\(paymentType)",
                        "status":"\(status)",
                        "deliveryToAdminAmount":"\(self.viewModel.chargeDataArr[0].data?.deliveryToAdminAmount ?? 0)",
                        "chargeId":"\(self.viewModel.chargeDataArr[0].data?.chargeData?.id ?? "")",
                        "deliveryType":self.deliveryType ?? ""
                ] as [String:Any]
            self.viewModel.getpaymentApiSrvice(passDict: dict)
            self.closureSetUp()
        }
    }
  
}
