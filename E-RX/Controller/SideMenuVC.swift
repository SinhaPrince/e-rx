//
//  SideMenuVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class SideMenuVC: UIViewController {

    //MARK:- OUtlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_MobileNumber: UILabel!
    @IBOutlet var headerTable: UIView!
    @IBOutlet var footerTable: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    
    //MARK:- Variables
    
    let obj = CommonClass.sharedInstance
    let appDel = UIApplication.shared.delegate as! AppDelegate
    let arrMenuList = ["Home","Notifications","My Offers","My Prescriptions","Share App","Rate Us","Settings"]
    let arrMenuListImg = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "notoficatios"),#imageLiteral(resourceName: "past_order_h"),#imageLiteral(resourceName: "pres_s"),#imageLiteral(resourceName: "share"),#imageLiteral(resourceName: "rate"),#imageLiteral(resourceName: "settings")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configUIElements()
        
    }
    
    
    //MARK:- Configure Components
    
    private func configUIElements(){
        
        let realm = try! Realm()
        let data = realm.objects(UserDataModel.self)
        
        obj.LblFontSizeName(name: data.first?.fullName ?? "", lbl: lbl_Name, textColor: .white, fontName: obj.BoldFont, fontSize: obj.BoldfontSize)
        obj.LblFontSizeName(name: data.first?.mobileNumber ?? "", lbl: lbl_MobileNumber, textColor: .white, fontName: obj.BoldFont, fontSize: obj.BoldfontSize)
        self.imgView.clipsToBounds = true
        self.imgView.setImage(withImageId: data.first?.profilePic ?? "", placeholderImage: #imageLiteral(resourceName: "user_dummy"))
        tableView.tableHeaderView = headerTable
        
        self.configTableView()
    }

    //MARK:- ProfileBtn Action
    
    @IBAction func btn_Profile(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        kAppDelegate.navigator?.drawerController.setDrawerState(.closed, animated: false)
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
}

//MARK:- UITableView Delegate and DataSource

extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    
    //MARK:- configTableView
    
    private func configTableView(){
        
        tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    //MARK:- numberOfRowsInSection
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrMenuList.count
}

//MARK:- cellForRowAt
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        
        
        obj.LblFontSizeName(name: arrMenuList[indexPath.row], lbl: cell.lbl_Menu, textColor: indexPath.row == 3 ? obj.AppColorMost : obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.regularfontSize)
        cell.img_View.image = arrMenuListImg[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        appDel.navigator?.drawerController.setDrawerState(.closed, animated: false)
        
         if indexPath.row == 1 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "\(NotificationVC.self)") as! NotificationVC
            self.navigationController?.pushViewController(vc, animated: false)
         }else if indexPath.row == 2{
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "\(MyOfferVC.self)") as! MyOfferVC
            self.navigationController?.pushViewController(vc, animated: false)
            
         } else if indexPath.row == 3 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "\(MyPrescriptionVC.self)") as! MyPrescriptionVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }else if indexPath.row == 5 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "\(RateUsVC.self)") as! RateUsVC
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            self.navigationController?.present(vc, animated: false, completion: nil)
          
        }else if indexPath.row == 6 {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "\(SettingVC.self)") as! SettingVC
            self.navigationController?.pushViewController(vc, animated: false)
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screen.height/12
    }
    
    
}

let screen = UIScreen.main.bounds
