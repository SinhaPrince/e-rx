//
//  SearchProviderVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 21/07/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class SearchProviderVC: BaseViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables
    
    var providerArr : [NearByProviderData]?
    var tableArr : [NearByProviderData]?
    var filterArr : [NearByProviderData]?
    var nearByProviderDelegate : NearByProviderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //configTable(data: providerArr!)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchBar.delegate = self
        self.searchBar.placeholder = "Search Provider"
        self.searchBar.compatibleSearchTextField.textColor = AppColor.textColor
        self.searchBar.compatibleSearchTextField.backgroundColor = .white
        searchBar.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    @IBAction func btn_CloseTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SearchProviderVC:UITableViewDelegate,UITableViewDataSource{
    
    func configTable(data:[NearByProviderData]){
        
        self.tableView.register(UINib(nibName: "RegisteredProvoderCell", bundle: nil), forCellReuseIdentifier: "RegisteredProvoderCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableArr = data
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RegisteredProvoderCell", for: indexPath) as! RegisteredProvoderCell
        cell.item = self.tableArr?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            self.nearByProviderDelegate?.nearByProviderDetails(data: self.tableArr?[indexPath.row])
        }
        
    }
    
}

extension SearchProviderVC:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterContentForSearchText(searchText: searchText)
        self.configTable(data: filterArr ?? [NearByProviderData]())
    }
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        if searchText != "" {
            
            filterArr = (providerArr?.filter {data in
                
                return (data.fullName?.lowercased().contains(searchText.lowercased()) ?? false)
                
                } ?? [NearByProviderData]())
        }else{
            self.filterArr?.removeAll()
        }
        //else { self.filterArr = self.providerArr!}
    }

}

protocol NearByProviderDelegate {
    
    func nearByProviderDetails(data:NearByProviderData?)
    
}
