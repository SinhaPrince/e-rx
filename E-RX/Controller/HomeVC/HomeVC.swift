//
//  HomeVC.swift
//  E-RX
//
//  Created by macbook on 29/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import RealmSwift

class HomeVC: BaseViewController {

    //MARK:- Outlet
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var lbl_Address: UILabel!
    
    //MARK:- Variables
    var locationManager = CLLocationManager()
    var lat = String()
    var long = String()
    var addressType = String()
    let filter = GMSAutocompleteFilter()
    var imagePicker =  UIImagePickerController()
    
    //Maps Variables
    private var infoWindow = MapInfoWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        //Configure Map InfoWindow
        self.infoWindow = loadNiB()
        
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        hideNavigationBar(true)
        
        super.viewDidAppear(animated)
        
        //Near by Provider Api Calling...
        self.nearByProvider(lat: getInfoById().latitude ?? "", long: getInfoById().longitude ?? "")
        
        lbl_Address.font = AppFont.Bold.size(AppFontName.Lato, size: 22)
        lbl_Address.textColor = AppColor.appColor
        
        //Internet Checking Functionality
        defaultConfiguration()
        self.delegateWarningMessageView = self
        
     
        
    }
    
    @IBAction func btn_Menu(_ sender: UIButton) {
        
        kAppDelegate.navigator?.drawerController.setDrawerState(.opened, animated: false)
        
    }
    
    @IBAction func btn_Search(_ sender:UIButton){
        
        if self.viewModel.nearByProviderArr.count > 0{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchProviderVC") as! SearchProviderVC
            if #available(iOS 13.0, *) {
                vc.modalPresentationStyle = .formSheet
            }
            vc.providerArr = self.viewModel.nearByProviderArr[0].data
            vc.nearByProviderDelegate = self
            self.present(vc, animated: true, completion: nil)
        }else{
            
            self.nearByProvider(lat: getInfoById().latitude ?? "", long: getInfoById().longitude ?? "")
            
        }
        
    }
    
    @IBAction func btn_Notification(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "\(NotificationVC.self)") as! NotificationVC
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    @IBAction func btn_CameraTap(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeCameraPopUpVC") as! HomeCameraPopUpVC
        vc.delegateCamAndGall = self
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        self.present(vc, animated: false, completion: nil)
        
    }
      
}



//MARK:- Api implementation

extension HomeVC:cameraAndgalleryDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func camAndGallery(type: String) {
        
        type == "Camera" ? comClass.openCamera(imagePicker: imagePicker, vc: self):comClass.openGallery(imagePicker: imagePicker, vc: self)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
      imagePicker.dismiss(animated: true, completion: nil)
            
            if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                
                if let imageData = chosenImage.jpegData(compressionQuality: 1.0) as NSData? {
                    
                    self.pageSetup(imageData: imageData)
                    
                }else{
                    print("imageData nahi aaya gya")
                }
            } else{
                print("Something went wrong")
            }
        self.dismiss(animated: true, completion: nil)
        
    }
   
   
    
}


//MARK:- Current Location Method
//MARK:- Location Delegates

extension HomeVC: GMSMapViewDelegate,CLLocationManagerDelegate
{
    
    func initializeTheLocationManager()
    {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        MapView.delegate = self
        MapView.mapType = .terrain
        MapView.isMyLocationEnabled = true
        
    }
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        MapView.clear()
        let marker = GMSMarker()
        let camera = GMSCameraPosition.camera(withLatitude:latitude , longitude:longitude , zoom: 10.0)
        MapView.camera = camera
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.icon = iconMarker
        marker.title = titleMarker
        marker.map = MapView
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locationManager.location?.coordinate
        
        let location1 = CLLocation(latitude: location?.latitude ?? 0.0, longitude: location?.longitude ?? 0.0)
        
        location1.fetchCityAndCountry { (address, error) in
            
            guard let address = address, error == nil else { return }
                
            self.lat = (location?.latitude.description)!
            self.long = (location?.longitude.description)!
            self.lbl_Address.text = address
            
            self.MapView.settings.compassButton = true
            let mapInsets = UIEdgeInsets(top: 200.0, left: 30.0, bottom: 300.0, right: 10.0)
            self.MapView.padding = mapInsets
            if let nav_height = self.navigationController?.navigationBar.frame.height
            {
                let status_height = UIApplication.shared.statusBarFrame.size.height
                self.MapView.padding = UIEdgeInsets (top: nav_height+status_height,left: 0,bottom: 0,right: 0);
            }
            
            self.createMarker(titleMarker: address, iconMarker: #imageLiteral(resourceName: "map_pin"), latitude: location?.latitude ?? 0.0, longitude: location?.longitude ?? 0.0)
            //Near By Provider Service Api Calling
            self.loadMarkersFromDB(self.viewModel.nearByProviderArr[0].data)
             
        }
    
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        var markerData : NearByProviderData?
        var isInfoShow = false
        if let data = marker.userData as? NearByProviderData {
            markerData = data
            isInfoShow = true
        }
        
        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        
        if !isInfoShow{
            return false
        }
        
        infoWindow.item = markerData
        // Configure UI properties of info window
        infoWindow.MainView.alpha = 0.6
        infoWindow.layer.cornerRadius = 20
        infoWindow.layer.borderWidth = 2
        infoWindow.shadowRadius = 20
        infoWindow.shadowOffset = CGSize(width: 60, height: 60)
        infoWindow.shadowColor = .black
        infoWindow.shadowOpacity = 0.6
        infoWindow.borderColor = AppColor.appColor
        // Offset the info window to be directly above the tapped marker
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 150
        self.view.addSubview(infoWindow)
        return false
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 150
        }
    }
        
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
         if status == .authorizedWhenInUse {
                       //locationManager.requestLocation()
               }else if status == .denied || status == .notDetermined{
                   
                   locationManager.requestLocation()
        }
    }
    
    //TODO:- Custom Method to show user Location
    
    func showUserSettings() {
        guard let urlGeneral = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        UIApplication.shared.open(urlGeneral)
    }
    
    @available(iOS 10.0, *)
    func locationDenied(){
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                print("No access")
                
                //Popup show
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                
            case .restricted:
                print("No access")
            case .denied:
                print("No access")
                
                let settingAlert = UIAlertController(title: "Dr.Now", message: "Turn on your Location from the settings to help us locate you", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Settings", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    
                    self.showUserSettings()
                }
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                    UIAlertAction in
                }
                
                settingAlert.addAction(okAction)
                settingAlert.addAction(cancelAction)
                self.present(settingAlert, animated: true, completion: nil)
                
            }
            
            
        } else {
            print("Location services are not enabled")
        }
        
    }
    
}

struct modelArr {
    var date: String
    var title: String
}

let dateArr: [modelArr] = [modelArr(date: "2020-08-04T17:47:53.321Z", title: "Hi"),
modelArr(date: "2020-08-06T09:47:33.489Z", title: "Hello"),
modelArr(date: "2020-08-07T09:47:47.191Z", title: "Hey")]

