//
//  HomeApiExtension.swift
//  E-RX
//
//  Created by SinhaAirBook on 27/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

extension HomeVC:TryAgainDeleagte,LoadWarningMessageView{
    
    func loadWarinigView() {
        let ref = self.showWarningMessageView(self.view, "Sorry,ERx requires an Internet Connection!")
        ref.delegate = self
    }
    
    func tryAgain() {
       self.nearByProvider(lat: self.lat, long: self.long)
    }
    
   
   // Initial page settings
    func pageSetup(imageData:NSData)  {
        
        self.showActivityIndicator(view: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.viewModel.getPrescriptionReqService(imageData:imageData, lat: self.lat, long: self.long)
            self.closureSetUp()
            
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = { [weak self] (message) in
            
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.handleError(message)
                
            }
        }
        
        viewModel.reloadList = {[weak self] () in
            
           // let data = self?.viewModel.nearByProviderArr[0].data
          //  self?.loadMarkersFromDB(data)
            self?.initializeTheLocationManager()
            let ref = self?.showWarningMessageView()
            ref?.removeFromSuperview()
            
        }
       
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
               // self?.hideActivityIndicator()
            }
        }
    }

    // Near By Provider
    
    func nearByProvider(lat:String,long:String){
       
        self.showActivityIndicator(view: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.viewModel.getNearByProviderService(passDict: ["latitude" : lat,"longitude":long])
            self.closureSetUp()
            
        }
        
    }
    
    
   
}

extension HomeVC:NearByProviderDelegate{
    
    func nearByProviderDetails(data: NearByProviderData?) {
        
        self.createMaker(data: data, isCameraPoint: true)
       
    }
    
}

//MARK:- Custom Marker InfoWindow Setup
//MARK:-

extension HomeVC{
    
    func loadMarkersFromDB(_ data:[NearByProviderData]?) {
        
       // self.MapView.clear()
        
        for i in 0..<(data?.count ?? 0){
            
            self.createMaker(data: data?[i], isCameraPoint: false)
        }
        
    }
    
    func createMaker(data: NearByProviderData?,isCameraPoint:Bool){
        
        // Get coordinate values from DB
        let latitude = data?.latitude
        let longitude = data?.longitude
        
        DispatchQueue.main.async {
            
            let marker = GMSMarker()
            let camera = GMSCameraPosition.camera(withLatitude:latitude ?? 0.0 , longitude:longitude ?? 0.0 , zoom: 10.0)
            if isCameraPoint{
              self.MapView.camera = camera
            }
            marker.icon = UIImage(named: "map_icn")
            marker.position = CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0)
            marker.map = self.MapView
            // *IMPORTANT* Assign all the spots data to the marker's userData property
            marker.userData = data
        }
    }
    
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> MapInfoWindow{
        let infoWindow = MapInfoWindow.instanceFromNib() as! MapInfoWindow
        return infoWindow
    }
}
