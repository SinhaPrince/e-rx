//
//  AddCardVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class AddCardVC: UIViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var txtCardExpiryDate: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtNameOnCard: UITextField!
    
    @IBOutlet weak var btn_Proceed: UIButton!
    @IBOutlet weak var lbl_PleaseFillHeader: UILabel!
    @IBOutlet weak var lbl_NaviHeader: UILabel!
    
    //MARK:- Variables
    let obj = CommonClass.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configureView()
        
    }
    
    //MARK:-Configure UiElements
    // =====  TextField Padding ======
    func configureView(){
        
        
        let paddingNameCard = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtNameOnCard.frame.height))
        txtNameOnCard.leftView = paddingNameCard
        txtNameOnCard.leftViewMode = UITextField.ViewMode.always
        
        
        let paddingCardNumber = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtCardNumber.frame.height))
        txtCardNumber.leftView = paddingCardNumber
        txtCardNumber.leftViewMode = UITextField.ViewMode.always
        
        let paddingCardExpiryDate = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtCardExpiryDate.frame.height))
        txtCardExpiryDate.leftView = paddingCardExpiryDate
        txtCardExpiryDate.leftViewMode = UITextField.ViewMode.always
        
        let paddingCardCVV = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtCVV.frame.height))
        txtCVV.leftView = paddingCardCVV
        txtCVV.leftViewMode = UITextField.ViewMode.always
        
       obj.LblFontSizeName(name: "Add New Card", lbl: lbl_NaviHeader, textColor: .white, fontName: obj.BoldFont, fontSize: obj.BoldfontSize)
        obj.LblFontSizeName(name: "Please fill the following details", lbl: lbl_PleaseFillHeader, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        obj.txtFldFontSizeConfig(txtFld: txtNameOnCard, placeHolderText: "Name on Card", fontName: obj.RegularFont, fontSize: obj.regularfontSize, textColor: obj.textDarkGray)
        obj.txtFldFontSizeConfig(txtFld: txtCardNumber, placeHolderText: "Card Number", fontName: obj.RegularFont, fontSize: obj.regularfontSize, textColor: obj.textDarkGray)
        obj.txtFldFontSizeConfig(txtFld: txtCardExpiryDate, placeHolderText: "Card Expiry Date", fontName: obj.RegularFont, fontSize: obj.regularfontSize, textColor: obj.textDarkGray)
        obj.txtFldFontSizeConfig(txtFld: txtCVV, placeHolderText: "CVV", fontName: obj.RegularFont, fontSize: obj.regularfontSize, textColor: obj.textDarkGray)
        obj.setDataOnButton(btn: btn_Proceed, text: "Proceed", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: .white, image: UIImage(), backGroundColor: obj.AppColorMost, aliment: .center)
        
    }
    
    @IBAction func btnProceedTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func btnbackTap(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
}
