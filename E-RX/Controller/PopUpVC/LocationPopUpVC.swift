//
//  LocationPopUpVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 02/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class LocationPopUpVC: BaseViewController {
    
    
    //MARK:- Outlet
    
    @IBOutlet weak var btnChecked: UIButton!
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var lbl_AskMe: UILabel!
    @IBOutlet weak var btn_Allow: UIButton!
    @IBOutlet weak var btn_DontAllow: UIButton!
    
    let obj = CommonClass.sharedInstance
    var delegate : backToMain?
    var nav = UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        obj.LblFontSizeName(name: "Allow E-RX to access and manage your Location", lbl: lbl_Header, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        
        obj.setDataOnButton(btn: btn_Allow, text: "Allow", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: #colorLiteral(red: 0.09047914296, green: 0.4063181877, blue: 0.8892967105, alpha: 1), image: UIImage(), backGroundColor: .clear, aliment: .center)
        obj.setDataOnButton(btn: btn_DontAllow, text: "Don't Allow", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: #colorLiteral(red: 0.9297258258, green: 0.2434281707, blue: 0.2003417313, alpha: 1), image: UIImage(), backGroundColor: .clear, aliment: .center)
        obj.LblFontSizeName(name: "Do Not ask again", lbl: lbl_AskMe, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.regularfontSize)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    //MARK:-UIButton Action
    
    //MARK:- 
    @IBAction func btnCheckedTap(_ sender: UIButton) {
        
        if !sender.isSelected {
            let image = UIImage(named: "radio_un")
            btnChecked.setImage(image, for: .normal)
            sender.isSelected = true
            UserDefaults.standard.set(false, forKey: "isLocationAllow")
        } else {
            
            let image = UIImage(named: "checked")
            btnChecked.setImage(image, for: .normal)
            sender.isSelected = false
            UserDefaults.standard.set(true, forKey: "isLocationAllow")
        }
    }
    
    @IBAction func btnDontAllowTap(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func btnAllowTap(_ sender: Any) {
        
        //weak var pvc = self.presentingViewController
        self.dismiss(animated: false, completion:{
            
            self.delegate?.back()
            
        })
        
        
    }
    
    
}

protocol backToMain {
    
    func back()
}
