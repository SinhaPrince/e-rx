//
//  AllowCamPopVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class AllowCamPopVC: BaseViewController {
    
    //MARK:- Outlet
    
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var btn_Allow: UIButton!
    @IBOutlet weak var btn_DontAllow: UIButton!
    
    //MARK:- Variables
    
    let obj = CommonClass.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        obj.LblFontSizeName(name: "Allow E-RX to access and manage your Camera Gallery", lbl: lbl_Header, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        
        obj.setDataOnButton(btn: btn_Allow, text: "Allow", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: #colorLiteral(red: 0.09047914296, green: 0.4063181877, blue: 0.8892967105, alpha: 1), image: UIImage(), backGroundColor: .clear, aliment: .center)
        obj.setDataOnButton(btn: btn_DontAllow, text: "Don't Allow", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: #colorLiteral(red: 0.9297258258, green: 0.2434281707, blue: 0.2003417313, alpha: 1), image: UIImage(), backGroundColor: .clear, aliment: .center)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    @IBAction func btn_TapAction(_ sender: UIButton) {
        
        if sender.tag == 0 {
            
            self.dismiss(animated: false, completion: nil)
            
        }else if sender.tag == 1 {
            
            self.dismiss(animated: false, completion: nil)
            
        }
        
    }
    
    
}
