//
//  NoRefundPolicyPopup.swift
//  E-RX
//
//  Created by SinhaAirBook on 05/08/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class NoRefundPolicyPopup: BaseViewController {

    @IBOutlet weak var sds: UILabel!
    @IBOutlet weak var tems: UILabel!
    @IBOutlet weak var btn_Counti: UIButton!
    @IBOutlet weak var transView: UIView!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var topView: UIView!
    
    var delegate: termsConditionsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        comClass.LblFontSizeName(name: "Once you paid money to the provider, Neither you can made a request for refund nor can cancel.", lbl: sds, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 17)
        comClass.LblFontSizeName(name: "I Agreed to the Terms & Conditions.", lbl: tems, textColor: AppColor.textColor, fontName: comClass.RegularFont, fontSize: 14)
        comClass.setDataOnButton(btn: btn_Counti, text: "Continue", font: comClass.BoldFont, size: 16, textcolor: .white, image: UIImage(), backGroundColor: .clear, aliment: .center)
        self.btnTerms.isSelected = false
        self.btnTerms.addTarget(self, action: #selector(termsBtnTap), for: .touchUpInside)
        self.btn_Counti.addTarget(self, action: #selector(btnContinueTap), for: .touchUpInside)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        addBlurOnView(view: topView)
    }
    
    @objc func termsBtnTap(){
        
        self.btnTerms.isSelected ? self.btnTerms.setImage(#imageLiteral(resourceName: "radio_unc"), for: .normal) : self.btnTerms.setImage(#imageLiteral(resourceName: "radio_c"), for: .normal)
        self.btnTerms.isSelected ? (self.tems.textColor = AppColor.textColor):(self.tems.textColor = AppColor.appColor)
        self.btnTerms.isSelected = !self.btnTerms.isSelected
    }

    @objc func btnContinueTap(){
        if self.btnTerms.isSelected{
            self.dismiss(animated: true) {
                self.delegate?.termsCondition()
            }
        }else{
            self.handleError("Please agree our terms and conditions then continue.")
        }
    }
   
}

protocol termsConditionsDelegate {
    func termsCondition()
}
