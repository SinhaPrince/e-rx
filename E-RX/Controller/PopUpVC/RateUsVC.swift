//
//  RateUsVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class RateUsVC: BaseViewController {
    
    //MARK:- Outlet
    
    @IBOutlet weak var lbl_RateHeader: UILabel!
    @IBOutlet weak var btn_Submit: UIButton!
    @IBOutlet weak var topView: UIView!
    
    //MARK:- Variable
    
    let obj = CommonClass.sharedInstance
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.addBlurOnView(view: topView)
    }
    
    //MARK:- Configure UIElements
    
    private func setConfiguration(){
        
        obj.LblFontSizeName(name: "Rate Us on Google Play Store", lbl: lbl_RateHeader, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        
        obj.setDataOnButton(btn: btn_Submit, text: "Submit", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: .white, image: UIImage(), backGroundColor: obj.AppColorMost, aliment: .center)
        
    }
    
    //MARK:- Button Action
    
    @IBAction func btn_Sub(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    
}
