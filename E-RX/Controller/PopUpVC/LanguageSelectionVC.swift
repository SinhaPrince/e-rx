//
//  LanguageSelectionVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class LanguageSelectionVC: BaseViewController {
    
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var btn_Eng: UIButton!
    @IBOutlet weak var btn_Arb: UIButton!
    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var topView: UIView!
    //MARK:- Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setConfiguration()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.addBlurOnView(view: topView)
    }
    
    //MARK:- Configure UIElements
    
    private func setConfiguration(){
        
        comClass.LblFontSizeName(name: "Language Selection", lbl: lbl_Header, textColor: comClass.textDarkGray, fontName: comClass.RegularFont, fontSize: comClass.BoldfontSize)
        
        setButtonInitilizaton(btnEng: btn_Eng, color1: AppColor.placeHolderColor, textColor: AppColor.textColor, btnArbic: btn_Arb, color2:AppColor.placeHolderColor, textColor2: AppColor.textColor)
        
        comClass.setDataOnButton(btn: btn_Next, text: "Next", font: comClass.RegularFont, size: comClass.regularfontSize, textcolor: #colorLiteral(red: 0.0862745098, green: 0.4039215686, blue: 0.8823529412, alpha: 1), image: UIImage(), backGroundColor: .clear, aliment: .center)
        btn_Arb.addTarget(self, action: #selector(languageBtnTap(_:)), for: .touchUpInside)
        btn_Eng.addTarget(self, action: #selector(languageBtnTap(_:)), for: .touchUpInside)
    }
    
    func setButtonInitilizaton(btnEng: UIButton, color1: UIColor, textColor: UIColor , btnArbic: UIButton , color2: UIColor , textColor2: UIColor){
        
        comClass.setDataOnButton(btn: btnEng, text: "English", font: comClass.BoldFont, size: comClass.BoldfontSize, textcolor: textColor, image: UIImage(), backGroundColor: color1, aliment: .center)
        btnEng.backgroundColor = color1
        btn_Eng.tag = 1
        btn_Arb.tag = 2
        btn_Arb.backgroundColor = color2
        comClass.setDataOnButton(btn: btn_Arb, text: "Arabic", font: comClass.BoldFont, size: comClass.BoldfontSize, textcolor: textColor2, image: UIImage(), backGroundColor: color2, aliment: .center)
        
    }
    
    //MARK:- Uibutton Action
    
    @IBAction func btn_NextTap(_ sender: UIButton) {
        
        let userData = realm.objects(UserDataModel.self)
        if userData.count != 0{
            kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window)
        }else{
            let appStartObj = AppStart()
            appStartObj.isLanguageScreenShow = "Yes"
            try! realm.write({
                realm.add(appStartObj)
                kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window)
            })
        }
    }
    
    @objc func languageBtnTap(_ sender:UIButton){
        
        if sender.tag == 1{
            
            setButtonInitilizaton(btnEng: btn_Eng, color1: AppColor.appColor, textColor: AppColor.whiteColor, btnArbic: btn_Arb, color2:AppColor.placeHolderColor, textColor2: AppColor.textColor)
            Localize.setCurrentLanguage(language: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            
        }else{
            
            setButtonInitilizaton(btnEng: btn_Eng, color1: AppColor.placeHolderColor, textColor: AppColor.textColor, btnArbic: btn_Arb, color2:AppColor.appColor, textColor2: AppColor.whiteColor)
            Localize.setCurrentLanguage(language: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            
        }
    }
}
