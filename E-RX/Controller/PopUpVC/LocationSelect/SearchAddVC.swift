//
//  SearchAddVC.swift
//  Dr.Now
//
//  Created by call soft on 12/06/2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

protocol passAddress {
    func backAddressToVC(Address:String,addressType:String,lat:String,long:String)
}

class SearchAddVC: BaseViewController {

    //MARK:- Outlet
    
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var currentBtn:UIButton!
    @IBOutlet var lbl_Header: UILabel!
    @IBOutlet var btn_Search: UIButton!
    
    //MARK:- Variable
    
    var locationManager = CLLocationManager()
    var lat = String()
    var long = String()
    var obj : passAddress?
    var addressType = String()
    let filter = GMSAutocompleteFilter()

    //MARK:- viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
        filter.type = .establishment
        filter.country = "IN"
        
    }
    
    //MARK:- viewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        initializeTheLocationManager()
        self.enableLocation()
        
    }
    
  
    func enableLocation(){
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                let alert = UIAlertController(title: "Your Location Services are Disabled" , message: "Turn on your location from the Settings to help us locate you.", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Settings",
                                              style: UIAlertAction.Style.destructive,
                                              handler: {(_: UIAlertAction!) in
                                                
                                                let settingsURL = URL(string: UIApplication.openSettingsURLString)!
                                                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    
    //MARK:-Btn Action
    //TODO:- Btn_Search Action
    @IBAction func btn_Search(_ sender: Any) {
        
        let placePickerController1 = GMSAutocompleteViewController()
        placePickerController1.delegate = self
        present(placePickerController1, animated: true, completion: nil)
        
    }
    
     //TODO:- Btn_SaveAddress Action
    @IBAction func SaveAddress(_ sender: Any) {
        
        if self.searchBar.text != ""{
            UserDefaults.standard.set( self.searchBar.text ?? "", forKey: "address")
            UserDefaults.standard.set(true, forKey: "CheckForAddress")
           
            self.dismiss(animated: true) {
                self.obj?.backAddressToVC(Address: self.searchBar.text ?? "", addressType: self.addressType, lat: self.lat , long: self.long )
            }
            
        }else{
            
            self.handleError("Please select your address.")
            
        }
        
       
        
    }
    
    //TODO:- Custom Method to show user Location
    
    func showUserSettings() {
        guard let urlGeneral = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        UIApplication.shared.open(urlGeneral)
    }
    
    //TODO:- Btn_CurrentLocationBnt Action
    
    @IBAction func CurrentLocationBnt(_ sender: Any) {
        
        self.initializeTheLocationManager()
        self.locationDenied()
       // self.currentBtn.setImage(#imageLiteral(resourceName: "location_track"), for: .normal)
        
    }
    
    
    
    
    
    
}

//MARK:- Current Location Method
//MARK:-

extension SearchAddVC:GMSAutocompleteViewControllerDelegate{
    
    //////Google places Delegates
    
        // Handle the user's selection.
        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
        {
            // Change map location
            self.MapView.clear()
            
            let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 16.0
            )
            
            //pickupLocation = place.formattedAddress!
            self.searchBar.text = place.formattedAddress!
            self.lat = place.coordinate.latitude.description
            self.long = place.coordinate.longitude.description
            
            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "map_pin"), latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
            
            self.MapView.camera = camera
            dismiss(animated: true, completion: nil)
        }
        
        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
        {
            print("Error: ", error.localizedDescription)
        }
        
        // User canceled the operation.
        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            dismiss(animated: true, completion: nil)
        }
        
        // Turn the network activity indicator on and off again.
        func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }

//MARK:- Location Delegates

extension SearchAddVC: GMSMapViewDelegate,CLLocationManagerDelegate
{
    
    func initializeTheLocationManager()
    {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        MapView.delegate = self
        MapView.mapType = .terrain
        MapView.isMyLocationEnabled = true
        
         
        
    }
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        MapView.clear()
        let marker = GMSMarker()
        let camera = GMSCameraPosition.camera(withLatitude:latitude , longitude:longitude , zoom: 10.0)
        MapView.camera = camera
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = MapView
        marker.isDraggable = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locationManager.location?.coordinate
        
        let location1 = CLLocation(latitude: location?.latitude ?? 0.0, longitude: location?.longitude ?? 0.0)
        
        location1.fetchCityAndCountry { (address, error) in
            
            guard let address = address, error == nil else { return }
                
            self.lat = (location?.latitude.description)!
            self.long = (location?.longitude.description)!
                self.searchBar.text = address
            
            self.MapView.settings.compassButton = true
            let mapInsets = UIEdgeInsets(top: 200.0, left: 30.0, bottom: 300.0, right: 10.0)
            self.MapView.padding = mapInsets
            if let nav_height = self.navigationController?.navigationBar.frame.height
            {
                let status_height = UIApplication.shared.statusBarFrame.size.height
                self.MapView.padding = UIEdgeInsets (top: nav_height+status_height,left: 0,bottom: 0,right: 0);
            }
            
            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "map_pin"), latitude: (location?.latitude)!, longitude: (location?.longitude)!)
            
        }
        
      
    
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let location1 = CLLocation(latitude: position.target.latitude , longitude: position.target.longitude )
        
        location1.fetchCityAndCountry { (address, error) in
            
            guard let address = address, error == nil else { return }
                
                self.lat = position.target.latitude.description
                self.long = position.target.longitude.description
                self.searchBar.text = address
        
            self.createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "map_pin"), latitude: (position.target.latitude), longitude: (position.target.longitude))
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
         if status == .authorizedWhenInUse {
                       //locationManager.requestLocation()
               }else if status == .denied || status == .notDetermined{
                   
                   locationManager.requestLocation()
        }
    }
    
    @available(iOS 10.0, *)
    func locationDenied(){
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                print("No access")
                
                //Popup show
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                
            case .restricted:
                print("No access")
            case .denied:
                print("No access")
                
                let settingAlert = UIAlertController(title: "Dr.Now", message: "Turn on your Location from the settings to help us locate you", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Settings", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    
                    self.showUserSettings()
                }
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                    UIAlertAction in
                }
                
                settingAlert.addAction(okAction)
                settingAlert.addAction(cancelAction)
                self.present(settingAlert, animated: true, completion: nil)
                
            }
            
            
        } else {
            print("Location services are not enabled")
        }
        
    }
    
    
}
