//
//  SelectAddressVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 02/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SelectAddressVC: BaseViewController,MKMapViewDelegate,serachTapDelegate {
    
    
    func serachTap() {
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        
        let searchbar = resultSearchController!.searchBar
        searchbar.sizeToFit()
        searchbar.placeholder = "Search for places"
       // navigationItem.searchController = resultSearchController
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        present(resultSearchController!, animated: true, completion: nil)
    }
    
    
    
    let locationManager = CLLocationManager()
    var resultSearchController:UISearchController? = nil
    var selectedPin:MKPlacemark? = nil
    var delegate : SelectAddressVCDelegate?
    var lat:String?
    var long:String?
    var address:String?
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var lbl_Address: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        mapView.delegate = self
        mapView.mapType = MKMapType.standard
        mapView.showsUserLocation = true
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       
        super.delagateSearch = self
        hideNavigationBar(false)
        setupNavigationBarTitle("Search Address", leftBarButtonsType: [.back], rightBarButtonsType: [.search])
        self.baseDelegate?.navigationBarButtonDidTapped(.search)
        let buttonItem = MKUserTrackingBarButtonItem(mapView: mapView)
        
        self.navigationItem.rightBarButtonItems = [buttonItem,getBarButtonItem(for: .search, isLeftBarButtonItem: false)]
       
    }
    
    @IBAction func btn_Countiniue(_ sender: UIButton) {
        
        if !self.lbl_Address.text!.isEmpty{
            
           self.delegate?.address(text: self.address!, lat: self.lat!, long: self.long!)
       self.navigationController?.popViewController(animated: false)
        }
        
    }
    
}



extension SelectAddressVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
         if status == .authorizedWhenInUse {
                       //locationManager.requestLocation()
               }else if status == .denied || status == .notDetermined{
                   
                   locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            
            let location1 = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            location1.fetchCityAndCountry {address, error in
                guard let address = address, error == nil else { return }
                self.lat = location.coordinate.latitude.description
                self.long = location.coordinate.longitude.description
                self.address = address
                self.fontAndColor(lbl: [self.lbl_Address], text: [self.address!])
            }
            
            mapView.setRegion(region, animated: true)
        }
        
    }
    
    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseIdentifier = "pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        // let customPointAnnotation = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named: "map_pin")
        
        
        return annotationView
    }
}

extension SelectAddressVC: HandleMapSearch {
    
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
    
        self.address = placemark.name
        self.lat = placemark.location?.coordinate.latitude.description
        self.long = placemark.location?.coordinate.longitude.description
        
        self.delegate?.address(text: self.address!, lat: self.lat!, long: self.long!)
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
        self.navigationController?.popViewController(animated: false)
    }
}
