//
//  HomePresSuccessPOPUPVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class HomePresSuccessPOPUPVC: UIViewController {

    
    //MARK:- Outlet
    
    @IBOutlet weak var lbl_Prescription: UILabel!
    @IBOutlet weak var trans_View: UIView!
    @IBOutlet weak var main_View: UIView!
    
    let obj = CommonClass.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        obj.LblFontSizeName(name: """
     Prescription uploaded successfully!
You will be notified once a provider accepts your request.
""", lbl: lbl_Prescription, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        
        let tapGenture = UITapGestureRecognizer()
        
        tapGenture.addTarget(self, action: #selector(self.tapGesMethod))
        
    self.trans_View.addGestureRecognizer(tapGenture)
        
    }
    
    
    @objc func tapGesMethod(){
        
        self.dismiss(animated: false, completion: nil)
    }

}
