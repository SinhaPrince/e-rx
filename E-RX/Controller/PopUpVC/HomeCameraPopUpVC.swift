//
//  HomeCameraPopUpVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class HomeCameraPopUpVC: BaseViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var lbl_Camera: UILabel!
    @IBOutlet weak var lbl_Gallery: UILabel!
    @IBOutlet weak var topView: UIView!
    
    //MARK:- Variables
    let obj = CommonClass.sharedInstance
    var delegateCamAndGall : cameraAndgalleryDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        hideNavigationBar(true)

        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.addBlurOnView(view: topView)
    }
    
    
    //MARK:- Configure UIElements
    
    private func setConfiguration(){
        
        obj.LblFontSizeName(name: "Choose Option", lbl: lbl_Header, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        obj.LblFontSizeName(name: "Camera", lbl: lbl_Camera, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        obj.LblFontSizeName(name: "Gallery", lbl: lbl_Gallery, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
       
        
    }
    
   
    
    //MARK:- UIButton Action
    
    @IBAction func btnCameraTap(_ sender: Any) {
        
        self.dismiss(animated: false) {
            self.delegateCamAndGall?.camAndGallery(type: "Camera")
        }
        
        
    }
    
    @IBAction func btnGalleryTap(_ sender: Any) {
        
        self.dismiss(animated: false) {
            self.delegateCamAndGall?.camAndGallery(type: "Gallery")
            
        }
        
    }
    
}
