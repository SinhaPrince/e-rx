//
//  ResetPasswordPopUpVC.swift
//  E-RX
//
//  Created by SinhaAirBook on 01/06/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class ResetPasswordPopUpVC: BaseViewController {
    
    
    //MARK:- Outlet
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lbl_Header: UILabel!
    @IBOutlet weak var btn_Done: UIButton!
    @IBOutlet weak var topView: UIView!
    
    //MARK:- Variables
    let obj = CommonClass.sharedInstance
    var userID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBackgroundView()
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.addBlurOnView(view: topView)
    }
    
    //MARK:- Configure UIElements
    // =====  TextField Padding ======
    func configureView(){
        
        let paddingNewPassword = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtPassword.frame.height))
        txtPassword.leftView = paddingNewPassword
        txtPassword.leftViewMode = UITextField.ViewMode.always
        
        
        let paddingConfirmPassword = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: txtConfirmPassword.frame.height))
        txtConfirmPassword.leftView = paddingConfirmPassword
        txtConfirmPassword.leftViewMode = UITextField.ViewMode.always
        
        obj.LblFontSizeName(name: "Reset your Password", lbl: lbl_Header, textColor: obj.textDarkGray, fontName: obj.RegularFont, fontSize: obj.BoldfontSize)
        obj.txtFldFontSizeConfig(txtFld: txtPassword, placeHolderText: "New Password", fontName: obj.RegularFont, fontSize: obj.BoldfontSize, textColor: obj.textDarkGray)
        obj.txtFldFontSizeConfig(txtFld: txtConfirmPassword, placeHolderText: "Confirm Password", fontName: obj.RegularFont, fontSize: obj.BoldfontSize, textColor: obj.textDarkGray)
        obj.setDataOnButton(btn: btn_Done, text: "Done", font: obj.BoldFont, size: obj.BoldfontSize, textcolor: .white, image: UIImage(), backGroundColor: obj.AppColorMost, aliment: .center)
        
    }
    
    
    
    @IBAction func btnPasswordEye(_ sender: Any) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    
    @IBAction func btnConfirmPasswordEye(_ sender: Any) {
        txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
    }
    
    @IBAction func btnDoneTap(_ sender: Any) {
        
        initialPageSetup()
        
    }
    
}

//MARK:- Forgot password api Service
//MARK:-
extension ResetPasswordPopUpVC{
    
    internal func initialPageSetup(){
        
        showActivityIndicator(view: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            let passDict = ["userId":self.userID ?? "","password":self.txtPassword.text ?? ""] as [String:Any]
            self.viewModel.getchangePasswordService(passDict: passDict)
            self.closureSetUp()
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = {[weak self] (message) in
            
            self?.handleError(message)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                
                self?.dismiss(animated: false) {
                    kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window)
                }
                
            }
            
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
    }
    
}
