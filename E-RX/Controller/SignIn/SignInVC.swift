//
//  SignInVC.swift
//  E-RX
//
//  Created by CST on 5/29/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import ADCountryPicker
import RealmSwift

class SignInVC: BaseViewController {
    
    
    //MARK:- Outlet
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var lbl_RememberMe: UILabel!
    @IBOutlet weak var btn_ForgotPass: UIButton!
    @IBOutlet weak var btn_Login: UIButton!
    @IBOutlet weak var btn_CreateAcc: UIButton!
    @IBOutlet weak var txt_countryCode:UITextField!
    @IBOutlet private weak var scrollView: UIScrollView!
   

    override func viewDidLoad() {
        super.viewDidLoad()
       
       initialSetup()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultConfiguration()
        hideNavigationBar(true)
        getRememberMeCredentialvalue()
        
       
        
    }

    
    //MARK:- Configure and Set UIName on UIElements
    //MARK:-
    func initialSetup(){
        
        configureView(txtFld: [txtPhoneNumber,txtPassword,txt_countryCode])
        txtFldFontSizeConfig(txtFld: [txtPhoneNumber,txt_countryCode,txtPassword], placeHolderText: [.phoneNo,.countryCode,.password])
        txt_countryCode.text = "+91"
        LblFontSizeName(name: [.rememberMe], lbl: [lbl_RememberMe])
        setDataOnButton(btn: [btn_ForgotPass,btn_Login,btn_CreateAcc], text: [.forgotPass,.login,.createAcc], textcolor: [AppColor.textColor,AppColor.whiteColor,AppColor.appColor])
        addDoneButtonOnKeyboard(txtFld: [txtPhoneNumber,txtPassword])
    }
    
    
    //MARK:- UIButton Action
    //MARK:- Password Fld text Show and Hide
    @IBAction func btnEyeTap(_ sender: Any) {
        
        txtPassword.isSecureTextEntry  = !txtPassword.isSecureTextEntry
 
        
    }
    //MARK:- Forgot password
    @IBAction func btnForgotPassword(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK:- Login Action
    @IBAction func btnLoginTap(_ sender: Any) {
         validation()
    }
    //MARK:- Create Acc Action
    @IBAction func btnCreateAccountTap(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .formSheet
        }
        vc.nav = self.navigationController
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func btnCountryCode(_ sender: UIButton) {
        
            let picker = ADCountryPicker()
            let pickerNavigationController = UINavigationController(rootViewController: picker)
            self.present(pickerNavigationController, animated: true, completion: nil)
            picker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
                print(dialCode)
                self.txt_countryCode.text = dialCode
                self.dismiss(animated: false, completion: nil)
            }
    }
    
    //MARK:- Remember Me Action
    @IBAction func btnCheckedTap(_ sender: UIButton) {
        
       sender.isSelected ? sender.setImage(#imageLiteral(resourceName: "radio_unc"), for: .normal) : sender.setImage(#imageLiteral(resourceName: "radio_c"), for: .normal)
       rememberMe()
       sender.isSelected = !sender.isSelected
    }

    
    //MARK:- Validation
    
    func validation(){
        
        var message = ""
        
        if (self.txt_countryCode.text?.isEmpty)!{
            message = Constant.countryCode
        }else if !isValid(userName: txtPhoneNumber.text!, regulerExp: RegularExtentionName.mobile){
            message = Constant.phoneNo
        }
//        else if !isValid(userName: txtPassword.text!, regulerExp: RegularExtentionName.passord){
//            message = Constant.password
//        }
        
        message == "" ? pageSetup() : handleError(message)
        
    }
    
    internal func rememberMe(){
        
        try! realm.write({
            let deletedNotifications = realm.objects(RememberMeModel.self)
            realm.delete(deletedNotifications)
            let data = RememberMeModel(userName: txtPhoneNumber.text ?? "", password: txtPassword.text ?? "")
            realm.add(data)
        })
    }

    func getRememberMeCredentialvalue(){
           let realm = try! Realm()
           let scope = realm.objects(RememberMeModel.self)
           
        if scope.count > 0{
            self.txtPhoneNumber.text = scope.first?.userName
            self.txtPassword.text = scope.first?.password
        }
    }
}

//MARK:- SignIn Api Call From ViewModel

extension SignInVC{
    
    // Initial page settings
    func pageSetup()  {
        
        showActivityIndicator(view: view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            
            let DToken = UserDefaults.standard.value(forKey: "DeviceToken") as? String ?? ""
            
            let passDict = ["countryCode":self.txt_countryCode.text ?? "","mobileNumber":self.txtPhoneNumber.text ?? "","password":self.txtPassword.text ?? "","deviceType":"iOS","deviceToken":DToken] as [String:Any]
            
            self.viewModel.getSignInService(passDict: passDict)
            self.closureSetUp()
            
        }
    }
    
   // Closure initialize
    func closureSetUp()  {
        
        viewModel.success = {[weak self] (message) in
            
            self?.handleError(message)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                
                let realm = try! Realm()
                let scope = realm.objects(WalkThorughShow.self)
                
                self?.rememberMe()
                
                if scope.first?.isAppStarted == "Yes"{
                   
                    kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
                    
                }else{
                   
                    self?.moveToNextViewC(name: "Main", withIdentifier: "WalkThrough")
                }
                
            }
            
        }
        
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
                // self?.hideActivityIndicator()
            }
        }
    }

    
}
