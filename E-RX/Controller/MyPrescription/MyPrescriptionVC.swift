//
//  MyPrescriptionVC.swift
//  E-RX
//
//  Created by macbook on 28/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class MyPrescriptionVC: BaseViewController {

    //MARK:- Outlet
   
    @IBOutlet weak var btn_OnGoing: UIButton!
    @IBOutlet weak var btn_Past: UIButton!
    @IBOutlet weak var scrollview: UIScrollView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

         intialSetUp()
         setName()
         Indicator.shared.showProgressView(view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        defaultConfiguration()
        self.delegateWarningMessageView = self
        setNavi()
    }
    
    //MARK:- Navigation Setup
    func setNavi(){
        
        hideNavigationBar(false)
        setupNavigationBarTitle("My Prescriptions", leftBarButtonsType: [.back], rightBarButtonsType: [])
        self.baseDelegate?.navigationBarButtonDidTapped(.back)
        
    }
 
    //MARK:- Set UIName on UIElements
    //MARK:-
    
    private func setName(){
        
        setDataOnButton(btn: [btn_OnGoing,btn_Past], text: [.onGoing,.past], textcolor: [AppColor.whiteColor,AppColor.textColor])
        
        
    }
    
    //MARK:- Btn Action
    
    //MARK:- On Going
    
    @IBAction func btnOnGo(_ sender: UIButton) {
        
        sender.backgroundColor = AppColor.appColor
        sender.setTitleColor(.white, for: .normal)
        btn_Past.backgroundColor = .clear
        btn_Past.setTitleColor(AppColor.textColor, for: .normal)
        
        animateScrollViewHorizontally(destinationPoint: CGPoint(x: 0 * self.view.frame.width, y: 0), andScrollView: self.scrollview, andAnimationMargin: 0)
        
    }
    
    //MARK:- Past

    @IBAction func btnPast(_ sender: UIButton) {
        
        sender.backgroundColor = AppColor.appColor
        sender.setTitleColor(.white, for: .normal)
        btn_OnGoing.backgroundColor = .clear
        btn_OnGoing.setTitleColor(AppColor.textColor, for: .normal)
        animateScrollViewHorizontally(destinationPoint: CGPoint(x: 1 * self.view.frame.width, y: 0), andScrollView: self.scrollview, andAnimationMargin: 0)

    }
    
    
    //MARK:- Custom Method
    
    func animateScrollViewHorizontally(destinationPoint destination: CGPoint, andScrollView scrollView: UIScrollView, andAnimationMargin margin: CGFloat) {
        
        var change: Int = 0;
        let diffx: CGFloat = destination.x - scrollView.contentOffset.x;
        var _: CGFloat = destination.y - scrollView.contentOffset.y;
        
        if(diffx < 0) {
            
            change = 1
        }
        else {
            
            change = 2
            
        }
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3);
        UIView.setAnimationCurve(.easeIn);
        switch (change) {
            
        case 1:
            scrollView.contentOffset = CGPoint(x: destination.x - margin, y: destination.y);
        case 2:
            scrollView.contentOffset = CGPoint(x: destination.x + margin, y: destination.y);
        default:
            return;
        }
        
        UIView.commitAnimations();
        
        let firstDelay: Double  = 0.3;
        let startTime: DispatchTime = DispatchTime.now() + Double(Int64(firstDelay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: startTime, execute: {
            
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.2);
            UIView.setAnimationCurve(.linear);
            switch (change) {
                
            case 1:
                scrollView.contentOffset = CGPoint(x: destination.x + margin, y: destination.y);
            case 2:
                scrollView.contentOffset = CGPoint(x: destination.x - margin, y: destination.y);
            default:
                return;
            }
            
            UIView.commitAnimations();
            let secondDelay: Double  = 0.2;
            let startTimeSecond: DispatchTime = DispatchTime.now() + Double(Int64(secondDelay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: startTimeSecond, execute: {
                
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1);
                UIView.setAnimationCurve(.easeInOut);
                scrollView.contentOffset = CGPoint(x: destination.x, y: destination.y);
                UIView.commitAnimations();
                
            })
        })
    }
    
    func intialSetUp()
    {
        self.scrollview.contentSize = CGSize(width: 2*self.view.frame.width,height: self.scrollview.frame.height);
        
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "OnGoingVC") as! OnGoingVC
        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "PastVC") as! PastVC
        
        self.addChild(vc1)
        self.addChild(vc2)
        
        self.loadScrollView()
    }
    
    func loadScrollView ()
    {
        print(self.children)
        for index in 0 ..< self.children.count
        {
            self.loadScrollViewWithPage(index);
        }
    }
    
    func loadScrollViewWithPage(_ page : Int) -> Void
    {
        if(page < 0)
        {
            return
        }
        if page >= self.children.count
        {
            return
        }
        let viewController: UIViewController? = (self.children as NSArray).object(at: page) as? UIViewController
        if(viewController == nil)
        {
            return
        }
        if(viewController?.view.superview == nil){
            
            var frame: CGRect  = self.scrollview.frame
            frame.origin.x = self.view.frame.width*CGFloat(page)
            frame.origin.y = 0;
            viewController?.view.frame = frame;
            self.scrollview.addSubview(viewController!.view);
        }
    }
    
}

//Show Warning
extension MyPrescriptionVC:TryAgainDeleagte,LoadWarningMessageView{
    
    func tryAgain() {
        
        let ref = self.showWarningMessageView()
        ref.removeFromSuperview()
        self.viewDidLoad()
        
    }
    
    func loadWarinigView() {
        let ref = self.showWarningMessageView(self.view, "Sorry,ERx requires an Internet Connection!")
        ref.delegate = self
    }
    
}
