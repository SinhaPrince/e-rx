//
//  OnGoingVC.swift
//  E-RX
//
//  Created by macbook on 28/05/20.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import RealmSwift

class OnGoingVC: BaseViewController {

    //MARK:- Outlet
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       initialMethod()
        
    }
   
}

//MARK:- UITableView Delegate and DataSource

extension OnGoingVC:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- numberOfRowsInSection
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.prescriptionListArray[0].data.count
    }
    
     //MARK:- cellForRowAt
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrescriptionCell", for: indexPath) as! PrescriptionCell
        //Send each row data on cell
        cell.setDataOnCell(data: viewModel.prescriptionListArray[0].data![indexPath.row],type:1)
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    //MARK:- DidSelect
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier:"PrescriptionIDVC") as! PrescriptionIDVC
        vc.prescriptionId = viewModel.prescriptionListArray[0].data[indexPath.row]._id
        self.navigationController?.pushViewController(vc, animated: false)
       
        
    }
    
    
}


//MARK:- Functions
extension OnGoingVC{
    
    func initialMethod() {
        
        tableView.register(UINib(nibName: "PrescriptionCell", bundle: nil), forCellReuseIdentifier: "PrescriptionCell")
       
        // Call pageSetup
           pageSetup()
    }
    
    
    // TableViewSetUp
    func tableViewSetup()  {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // Initial page settings
    func pageSetup()  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // API calling from viewmodel class
            
            let passDict = ["prescriptionType":"Ongoing"] as [String:Any]
            let header = ["Authorization":self.getInfoById().Token] as! [String:String]
            
            self.viewModel.getServicecall(URL: "prescriptionList", Parameters: passDict, Header: header)
            self.closureSetUp()
            
        }
    }
    
    // Closure initialize
    func closureSetUp()  {
        viewModel.reloadList = { [weak self] ()  in
            ///UI chnages in main tread
            DispatchQueue.main.async {
                
                self?.tableViewSetup()
                self?.tableView.reloadData()
                
            }
        }
        viewModel.errorMessage = { [weak self] (message)  in
            DispatchQueue.main.async {
                print(message)
                self?.handleError(message)
               // self?.hideActivityIndicator()
            }
        }
    }
}

