//
//  ATCClassicWalkthroughViewController.swift
//  DashboardApp
//
//  Created by Florian Marcu on 8/13/18.
//  Copyright © 2018 Instamobile. All rights reserved.
//

import UIKit

class ATCClassicWalkthroughViewController: UIViewController {
  
    
  //MARK:- Outlet
  @IBOutlet var containerView: UIView!
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var subtitleLabel: UILabel!
  @IBOutlet weak var btn_SKIP: UIButton!
    
  let model: ATCWalkthroughModel
  
  init(model: ATCWalkthroughModel,
       nibName nibNameOrNil: String?,
       bundle nibBundleOrNil: Bundle?) {
    self.model = model
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.image = UIImage.localImage(model.icon, template: true)
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    imageView.tintColor = .white
    
    titleLabel.textAlignment = .center
    subtitleLabel.textAlignment = .left
    titleLabel.text = model.title
    titleLabel.font = AppFont.Bold.size(AppFontName.Lato, size: 20)
    titleLabel.textColor = .white
    
    subtitleLabel.attributedText = NSAttributedString(string: model.subtitle)
    subtitleLabel.font = AppFont.Regular.size(AppFontName.Lato, size: 16)
    subtitleLabel.textColor = .white
    comClass.setDataOnButton(btn: btn_SKIP, text: model.btnTitle, font: comClass.BoldFont, size: 18, textcolor: .white, image: UIImage(), backGroundColor: .clear, aliment: .center)
    btn_SKIP.addTarget(self, action: #selector(skipTap(_:)), for: .touchUpInside)
    containerView.backgroundColor = UIColor(hexString: "#3068CC")
  }
    
    @objc func skipTap(_ sender:UIButton){
        kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
    }
    
}
