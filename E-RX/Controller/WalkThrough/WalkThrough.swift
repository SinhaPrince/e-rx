//
//  ViewController.swift
//  WalkthroughOnboarding
//
//  Created by Florian Marcu on 8/16/18.
//  Copyright © 2018 Instamobile. All rights reserved.
//

import UIKit
import RealmSwift

class WalkThrough: UIViewController,  ATCWalkthroughViewControllerDelegate{
    
    
  let walkthroughs = [
    ATCWalkthroughModel(title: "How to add a prescription", subtitle: "1 : Click on the camera icon. \n\n2 : Choose the picture from gallery or capture by camera. \n\n3 : Upload the prescription.", icon: "a",btnTitle: "SKIP"),
    ATCWalkthroughModel(title: "Choosing pick up delivery", subtitle: "1 : You will receive a notification once a provider accepts your prescription. \n\n2 : Once you accept the request from a provider and pay(if any),you can either choose to pick up yourself or the delivery option for additional AED 31.5. ", icon: "b",btnTitle: "SKIP"),
    ATCWalkthroughModel(title: "My Prescriptions", subtitle: "1 : You can check your prescription status from here,accept or refuse a request from the provider.", icon: "c",btnTitle: "Lets Go")
    
  ]
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let walkthroughVC = self.walkthroughVC()
        walkthroughVC.delegate = self
        self.addChildViewControllerWithView(walkthroughVC)
        
        let realm = try! Realm()
        let appStartObj = WalkThorughShow()
        appStartObj.isAppStarted = "Yes"
        try! realm.write({
            realm.add(appStartObj)
            
        })
    }
  
  func walkthroughViewControllerDidFinishFlow(_ vc: ATCWalkthroughViewController) {
    
    UIView.transition(with:self.view, duration: 1, options: .transitionFlipFromLeft, animations: {
      vc.view.removeFromSuperview()
      let viewControllerToBePresented = UIViewController()
      self.view.addSubview(viewControllerToBePresented.view)
    }, completion: { (true) in
        kAppDelegate.navigator = ApplicationNavigator(window: kAppDelegate.window, sendViewController: "HomeVC")
    })

  }
  
  fileprivate func walkthroughVC() -> ATCWalkthroughViewController {
    let viewControllers = walkthroughs.map { ATCClassicWalkthroughViewController(model: $0, nibName: "ATCClassicWalkthroughViewController", bundle: nil) }
    
    return ATCWalkthroughViewController(nibName: "ATCWalkthroughViewController",
                                        bundle: nil,
                                        viewControllers: viewControllers)
  }
}
